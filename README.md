# WeidnerTLR


## This folder contains the code used to analyze the data included in the paper
You will need some additional functions for the code to run smoothly. Custom functions that were specifically written for these analyses are included in this folder. Third-party functions are linked within the code.

Most of the codes are not designed to be a one-click-and-run because I like to check my data throughout the analysis. However, most of the codes are able to be run in one go.

You will find the data that is needed for statistical analysis in this repository.

### 01 Behavioural data analysis

s01_behav_ratings.R Code to process and analyze valence and arousal ratings for each patient and for all trials types. The code reproduces Supplementary Fig. 1A

s01_behav_recognition.R Code to process and analyze memory performance as reported in Supplementary Table 2. The code reproduces Supplementary Fig. 1B

### 02 Preprocessing and ERP/ time frequency analysis and cleaning of the electrophisiological data

s02_1preprocessing_lTLR.m Code to preprocess the scalp EEG data from the lTLR patients.

s02_1preprocessing_rTLR.m Code to preprocess the scalp EEG data from the rTLR patients.

s02_1preprocessing_HC.m Code to preprocess the scalp EEG data from the heatlhy controls.


###### note: the following time frequency extraction produces data that we will use for the ERP extraction. This is we are omitting trials based on time frequency artifacts, so run these first and then do the ERP stats

s02_2freqprocessing_lTLR.m Code to perform time frequency extraction and trial by trial visual inspection for rejecting artifacts in the lTLR patients.

s02_2freqprocessing_rTLR.m Code to perform time frequency extraction and trial by trial visual inspection for rejecting artifacts in the rTLR patients.

s02_2freqprocessing_HC.m  Code to perform time frequency extraction and trial by trial visual inspection for rejecting artifacts in the healthy controls.

s02_3ERP_extraction_plotting.m Code to perform ERP extraction in all groups. The code reproduces Figures 2-5A and 2-5C.



### 03 ERP and time frequency results and statistics 
s03_1ERP_export.m reproduces the export of single-trial ERP data for statistical analysis in R. 

s03_1GBA_export.m reproduces the export of single-trial time-frequency data for statistical analysis in Matlab.

s03_2ERP_analysis_plotting.R imports single-trial ERP data into R and analyses data with linear mixed models. The code reproduces Figures 2-5B.

s03_3GBA_permutations.m imports single-trial time frequency data into Matlab. It computes pointwise linear mixed models as well as respective model parameters for n permutations.

s03_4GBA_clusterstats.m imports permutation data from Matlab and runs cluster-based analyses on time-frequency data. The code reproduces Figure 7A and the topographies from Figure 7B. It also exports time frequency data for plotting in R. 

s03_4GBA_posthoc_plotting.R imports time-frequency cluster data into R for post-hoc tests and plotting. This reproduces the barplot in Figure 7B.

s03_5GBA_topos.m plots time-frequency topographies for negative and neutral stimuli for each group. The code reproduces Figure 6. 

### 04 Correlation analyses
s04_correlations.R reproduces the correlation analyses ran on the EEG data. The code reproduces Supplementary Fig. 2.


### Custom functions
glm_withinfreq_resec_parallel.m computes pointwise lm for observed time frequency data

permutation_test_glm.m computes n permutations and respective pointwise linear mixed models

shuffle_data.m is needed for the permutation tests

fit_model.m is needed for the lm tests in matlab

## Support
If you have any questions regarding the scripts or analyses, or if you encounter any bugs or errors, contact enya.weidner@uni-bielefeld.de


## Authors and acknowledgment
Publication authors: Enya Weidner, Lea Marie Reisch, Malena Mielke, Christian G. Bien, Johanna Kissler

Script author: Enya Weidner

We would like to thank Stephan Moratti, Alba Peris-Yagüe and Manuela Costa for helping with the analysis scripts for the time frequency analysis.




