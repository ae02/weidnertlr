
% Define the fit_model function outside of the main function
function coef_summary = fit_model(dataframe, f, t)
    subset_data = dataframe(dataframe.freq == f  & dataframe.timepoint == t, :);
    model1 = fitlme(subset_data, 'value ~ group*emo + (1|subj)', 'FitMethod','REML','DummyVarCoding','effects');
    coef_summary = model1.Coefficients.tStat(2:6);
end

