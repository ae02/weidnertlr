function results = glm_withinfreq_resec_parallel(num_chan, num_freq, num_time)
    % Initialize cell arrays to store the results for each coefficient
    results = cell(1, 5);

    % Preallocate matrices for each coefficient
    statobs_ltlrvscon = zeros(num_chan, num_freq, num_time);
    statobs_rtlrvscon = zeros(num_chan, num_freq, num_time);
    statobs_emo = zeros(num_chan, num_freq, num_time);
    statobs_emo_ltlrvscon = zeros(num_chan, num_freq, num_time);
    statobs_emo_rtlrvscon = zeros(num_chan, num_freq, num_time);


    % Use parfor loop to iterate over channels in parallel
    parfor c = 1:num_chan
%         dataframe = [];
%         files = dir(fullfile(path, strcat('/*_c', num2str(c), '.csv')));
%         for k = 1 : length(files)
%             dataframe = [dataframe; readtable(fullfile(files(k).folder, files(k).name))];
%         end
        data = load(strcat('channel_',num2str(c),'.mat'));
        
        data.dataframe.emo = categorical(data.dataframe.emo);
        data.dataframe.group = categorical(data.dataframe.group);
        data.dataframe.subj = categorical(data.dataframe.subj);

        % Iterate over frequencies and timepoints
        for f = 1:num_freq
            for t = 1:num_time
                subset_data = data.dataframe(data.dataframe.freq == f  & data.dataframe.timepoint == t, :);
                model1 = fitlme(subset_data, 'value ~ group*emo + (1|subj)', 'FitMethod','REML','DummyVarCoding','effects');
                statobs_ltlrvscon(c, f, t) = model1.Coefficients.tStat(3);
                statobs_rtlrvscon(c, f, t) = model1.Coefficients.tStat(4);
                statobs_emo(c, f, t) = model1.Coefficients.tStat(2);
                statobs_emo_ltlrvscon(c, f, t) = model1.Coefficients.tStat(5);
                statobs_emo_rtlrvscon(c, f, t) = model1.Coefficients.tStat(6);
            end
        end
    end

    % Store each matrix in a separate cell of the cell array
    results{2} = statobs_ltlrvscon;
    results{3} = statobs_rtlrvscon;
    results{1} = statobs_emo;
    results{4} = statobs_emo_ltlrvscon;
    results{5} = statobs_emo_rtlrvscon;
end


