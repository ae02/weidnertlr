function results = permutation_test_glm(num_chan, num_freq, num_time, num_permutations)
    % Initialize cell arrays to store the results for each coefficient
    results = cell(1, 5);

    %path = pwd;

    % Use parfor loop to iterate over channels
    parfor c = 1:num_chan
%         dataframe = [];
%         files = dir(fullfile(path, strcat('/*_c', num2str(c), '.csv')));
%         for k = 1 : length(files)
%             dataframe = [dataframe; readtable(fullfile(files(k).folder, files(k).name))];
%         end
        data = load(strcat('channel_',num2str(c),'.mat'));
        

        data.dataframe.emo = categorical(data.dataframe.emo);
        data.dataframe.group = categorical(data.dataframe.group);
        data.dataframe.subj = categorical(data.dataframe.subj);

        
        % Initialize arrays for permuted results
        perm_statrnd_ltlrvscon = zeros(num_freq, num_time, num_permutations);
        perm_statrnd_rtlrvscon = zeros(num_freq, num_time, num_permutations);
        perm_statrnd_emo = zeros(num_freq, num_time, num_permutations);
        perm_statrnd_int_ltlrvscon = zeros(num_freq, num_time, num_permutations);
        perm_statrnd_int_rtlrvscon = zeros(num_freq, num_time, num_permutations);

        for f = 1:num_freq
            for t = 1:num_time
                subset_data = data.dataframe(data.dataframe.freq == f & data.dataframe.timepoint == t, :);

                for perm = 1:num_permutations
                    % Create separate variables for each permutation
                    shuffled_data_emo_perm = shuffle_data(subset_data, 'emo');
                    shuffled_data_group_perm = shuffle_data(subset_data, 'group');
                    shuffled_data_int_perm = subset_data;
                    shuffled_data_int_perm.emo = shuffled_data_emo_perm.emo;
                    shuffled_data_int_perm.group = shuffled_data_group_perm.group;
                    

                    perm_model_group = fitlme(shuffled_data_group_perm, 'value ~ group*emo + (1|subj)', 'FitMethod', 'REML', 'DummyVarCoding', 'effects');
                    perm_statrnd_ltlrvscon(f, t, perm) = perm_model_group.Coefficients.tStat(3);
                    perm_statrnd_rtlrvscon(f, t, perm) = perm_model_group.Coefficients.tStat(4);
                    
                    perm_model_emo = fitlme(shuffled_data_emo_perm, 'value ~ group*emo + (1|subj)', 'FitMethod', 'REML', 'DummyVarCoding', 'effects');
                    perm_statrnd_emo(f, t, perm) = perm_model_emo.Coefficients.tStat(2);
                    
                    perm_model_int = fitlme(shuffled_data_int_perm, 'value ~ group*emo + (1|subj)', 'FitMethod', 'REML', 'DummyVarCoding', 'effects');
                    perm_statrnd_int_ltlrvscon(f, t, perm) = perm_model_int.Coefficients.tStat(5);
                    perm_statrnd_int_rtlrvscon(f, t, perm) = perm_model_int.Coefficients.tStat(6);
                end
            end
        end
        
        % Store permuted results in the structure
        results{c} = struct(...
            'ltlrvscon', perm_statrnd_ltlrvscon, ...
            'rtlrvscon', perm_statrnd_rtlrvscon, ...
            'emo', perm_statrnd_emo, ...
            'int_ltlrvscon', perm_statrnd_int_ltlrvscon, ...
            'int_rtlrvscon', perm_statrnd_int_rtlrvscon ...
        );
    end
end
