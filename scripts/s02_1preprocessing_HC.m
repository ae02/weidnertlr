%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% this script preprocesses raw EEG data of the TLR paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% first set up fieldtrip toolbox, for more information visit: https://www.fieldtriptoolbox.org/
%addpath PATHTOSTARTUPFUNCTION
%ft_startup

% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR


%% processing with ICA - remove only eye blinks, the rest of trials are inspected with manual rejction based on range

datadir = '...\raw_data\';
participants = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'};

%pre-allocate memory to save time
resampled_con = cell(1,21);
resampled_con{1,21} = [];

for VP=1:length(participants)
    data                   = strcat(datadir,'CON_', num2str(participants{VP}),'.bdf');
    cfg                    = [];
    cfg.dataset            = data;
    cfg.channel            = {'all','-EXG*', '-Status', '-GSR1','-GSR2','-Erg1','-Erg2','-Resp','-Plet','-Temp'};             % define channel type
    cfg.reref              = 'yes';                          % rereference data
    cfg.refchannel         = 'all';
    cfg.refmethod          = 'avg';
    cfg.hpfilter           = 'yes';                          % enable high-pass filtering
    cfg.lpfilter           = 'yes';                          % enable low-pass filtering
    cfg.hpfiltord          = 1;
    cfg.hpfreq             = 0.1;                            % set up the frequency for high-pass filter
    cfg.lpfreq             = 100;                             % set up the frequency for low-pass filter
    cfg.dftfilter          = 'yes';
    cfg.dftfreq            = 49:51;
    cfg.detrend            = 'yes';
    data2                  = ft_preprocessing(cfg);          % read raw data
  
    % define trials
    triggercode = [
      6
      7
      106
      107
      ];

    cfg                    = [];
    cfg.trialdef.prestim   = 1.5;                   % in seconds
    cfg.trialdef.poststim  = 2.101;                   % in seconds
    cfg.trialdef.eventtype = 'STATUS';            % get a list of the available types
    cfg.trialdef.eventvalue = triggercode;                    % trigger values
    cfg.dataset            = data;             % set the name of the dataset
    cfg_tr_def             = ft_definetrial(cfg);   % read the list of the specific stimulus

    % segment data according to the trial definition
    data_trials                    = ft_redefinetrial(cfg_tr_def, data2);

    cfg = [];
    cfg.resamplefs = 500;
    resampled_con{str2num(participants{VP})} = ft_resampledata(cfg,data_trials);
end

%save resampled_CON.mat '-v7.3' resampled_con;

%% run ica
% pre allocate memory
comp = cell(1,length(resampled_con));
comp{1,length(resampled_con)} = [];

for VP = 1:length(resampled_con)
cfg        = [];
cfg.runica.pca = 127;  
cfg.method = 'runica'; % this is the default and uses the implementation from EEGLAB
comp{VP} = ft_componentanalysis(cfg, resampled_con{VP}); 
end

%save comp_con.mat '-v7.3' comp;

% plot components and decide which ones you want to omit
% NOTE: the components change every time so the components that I omitted are
% not necessarily numbered the same the next time you run the analyses

cfg = [];
cfg.component = 1:40;       % specify the component(s) that should be plotted
cfg.layout    = 'biosemi128.lay'; % specify the layout file that should be used for plotting
cfg.comment   = 'no';
cfg.viewmode = 'component';
cfg.interactive = 'no';
cfg.continuous = 'no'; % view the data in it's segments
cfg.channel = 'all'; % view all the component in one screen
cfg.legend = 'yes';

for VP = 1:length(resampled_con)
    figure; topo_plot = ft_topoplotIC(cfg, comp{VP});
end
%
for VP = 1:length(resampled_con)
    cfg = [];
    cfg.layout = 'biosemi128.lay'; % specify the layout file that should be used for plotting
    cfg.viewmode = 'component';
    browse =ft_databrowser(cfg, comp{VP});
end

% note down components for each participants 
components{1} = [1 2 11 15 16 20 23 26 39]; 
components{2} = [1 3 8 12 14 15 16 17 23 38]; 
components{3} = [2 7 30];
components{4} = [10 11 12 13 18 19 20 22 27 34];
components{5} = [1 5 29 30 31 32 37 43];
components{6} = [1 2 8 25 46]; 
components{7} = [1 7 8 15 16 14 37];
components{8} = [1 2 4 6 8 30 36 55];
components{9} = [1 6 7 8 15 17 19 23 33 44 50];
components{10} = [1 2 9 10 11 13 18 22];
components{11} = [1 3 23 46];
components{12} = [1 2 3 4 6 8 9 15 22 33]; 
components{13} = [1 3 14 10 12 48]; 
components{14} = [1 2 8 24 10 20];
components{15} = [1 5 16]; 
components{16} = [1 3 5 8 19 21 38]; 
components{17} = [1 38 9 12 21 29]; 
components{18} = [9 1 20 26 40]; 
components{19} = [1 3 37 4 18 19 24 27 37 53 72];
components{20} = [1 2 8 13 5 13 16]; 
components{21} = [1 15 4]; 

%% artifact correction

% construct neighbours for channel interpolation
data                = resampled_con{1};
cfg_neighb          = [];
cfg_neighb.channel  = {'all'};
cfg_neighb.method   = 'triangulation';
cfg_neighb.layout   = 'biosemi128.lay';
cfg.feedback        = 'yes';
neighbours          = ft_prepare_neighbours(cfg_neighb, data);

% load('elecs.mat') % load 3d electrode positions from file

% reject components and do one last trial rejection and channel
% interpolation

% pre allocate memory
reject_con = cell(1,length(resampled_con));
reject_con{1,length(resampled_con)} = [];

for VP = 1:length(resampled_con)
    cfg = [];
    cfg.component = components{VP}; % to be removed component(s)
    data_ica_faces = ft_rejectcomponent(cfg, comp{VP}, resampled_con{VP});

    cfg = [];
    cfg.method = 'summary';
    cfg.metric = 'range';
    cfg.keepchannel = 'repair';
    cfg.keeptrial = 'no';
    cfg.neighbours = neighbours;
    cfg.layout = 'biosemi128.lay';
    cfg.elec = elecs;
    reject_con{VP} = ft_rejectvisual(cfg, data_ica_faces);
end
save('reject_resection_con.mat', '-v7.3', 'reject_con');

