%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% this script extracts time frequency information and performs an
%%%%% additional artifact correction
%%%%% credit: much of this code is based on scripts provided by Stephan Moratti, Alba Peris Yag�e and
%%%%% Manuela Costa - thanks a lot!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% first set up fieldtrip toolbox, for more information visit: https://www.fieldtriptoolbox.org/
%addpath PATHTOSTARTUPFUNCTION
%ft_startup

% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR

%% FREQUENCY ANALYSIS RESECTION DATA (FACES) 
ft_startup

load('..\reject_resection_ahr.mat')

% pre allocate memory
ref_AHR = cell(1,length(reject_ahr));
ref_AHR{1,length(reject_ahr)} = [];

% downsample data
for VP = 1:length(reject_ahr)
    cfg = [];
    cfg.resamplefs = 200;
    ref_AHR{VP} = ft_resampledata(cfg, reject_ahr{VP});
end

clear cleaned;
%% TF1 %% - this is used for artifact correction

%%%% freq analysis
foi =[]; fw = []; toi = []; 


foi = 30:5:90; 
tw  = 0.2*ones(length(foi),1)'; % make sure that the window length fits integaer values of the rayleigh freq
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

  k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
    tap=k-1;
else
    tap=floor(k);
end    

%%pre-allocate memory to save time
freq_neg_AHR = cell(1,length(ref_AHR));
freq_neg_AHR{1,length(ref_AHR)} = [];

freq_ntr_AHR = cell(1,length(ref_AHR));
freq_ntr_AHR{1,length(ref_AHR)} = [];

for VP = 1:length(ref_AHR)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; %length of time window in seconds
    cfg.tapsmofrq = fw; %the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; %AP
    cfg.pad       = 'maxperlen'; %length in seconds to which the data can be padded out
    cfg.keeptrials = 'no'; % if you want to look at single subejct data
   
    data = ref_AHR{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);

    cfg=[];
    cfg.avgoverrpt = 'yes';
    freq_neg_AHR{VP} = ft_selectdata(cfg,freq_ses11);
    freq_ntr_AHR{VP} = ft_selectdata(cfg,freq_ses12);
    
    clear HFfreq*
end


clear freq_ses*;
clear freq_tmp*;
clear freq_HF*;


%% single subjects for artifact checks
% i do this to get a general idea of the data on a single subject level,
% this is not really necessary
for VP = 1:length(freq_neg_AHR)
    cfg=[];
    cfg.keepindividual='yes';
    f_neg = ft_freqgrandaverage(cfg,freq_neg_AHR{VP});
    f_ntr = ft_freqgrandaverage(cfg,freq_ntr_AHR{VP});

    xfreq_neg_AHR = ft_freqdescriptives([],f_neg);
    xfreq_ntr_AHR = ft_freqdescriptives([],f_ntr);

    cfg=[];
    cfg.latency = [0 0.8];
    cfg.frequency = [35 90];
    xfreq_neg_AHR = ft_selectdata(cfg,xfreq_neg_AHR);
    xfreq_ntr_AHR = ft_selectdata(cfg,xfreq_ntr_AHR);


    %% PLOTTING
    cfg = [];
    cfg.figure = 'gcf';
    cfg.xlim = [0 0.8];
    cfg.zlim = [-0.2 0.2];
    cfg.parameter = 'powspctrm'; %change it statBL_H.stat to plot the stats instead
    cfg.colormap = colormap((parula(100)));
    cfg.maskstyle = 'outline';
    cfg.colorbar = 'no';
    cfg.title = '';
    cfg.layout = 'biosemi128.lay';

    %%% singles
    figure('Position',[181 389 1101 572], 'Color', [1 1 1]);
    subplot(121);
    ft_multiplotTFR(cfg,xfreq_neg_AHR);
    %tit=title(['attend to angry (angry)']);set(findobj(tit,'type','text'),'FontSize',15);
    tit = title('AHR - negative');
    subplot(122);
    ft_multiplotTFR(cfg,xfreq_ntr_AHR);
    %tit=title(['attend to angry (angry)']);set(findobj(tit,'type','text'),'FontSize',15);
    tit = title('AHR - neutral');
    grid on
end

%% CLEAN DATA IN FREQUENCY DOMAIN

  foi = 30:5:100; 
  tw  = 0.2*ones(length(foi),1)';
  fw  = 10*ones(length(foi),1)';
  toi = 'all'; 
  
  k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
    tap=k-1;
else
    tap=floor(k);
end    

%pre-allocate memory to save time
Hfreq_base = cell(1,length(ref_AHR));
Hfreq_base{1,length(ref_AHR)} = [];

  for VP = 1:length(ref_AHR)

      cfg = [];
      cfg.output    = 'pow';
      cfg.channel   = 'all';
      cfg.method    = 'mtmconvol';
      cfg.taper     = 'dpss';
      cfg.keeptrials = 'yes'; 
      cfg.foi       = foi; 
      cfg.t_ftimwin = tw; % length of time window in seconds
      cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
      %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
      cfg.toi       = toi; 
      cfg.pad       = 'maxperlen'; %length in seconds to which the data can be padded out
      Hfreq = ft_freqanalysis(cfg, ref_AHR{VP});

      cfg=[];
      cfg.baseline=[-0.6 -0.1];
      cfg.baselinetype = 'relchange';
      Hfreq_base{VP} = ft_freqbaseline(cfg, Hfreq);

    clear Hfreq
  end

  
% the following code plots each trials (128 channels) so you can browse
% through each trial manually and mark artifacts as you go
 for VP =1:length(ref_AHR)

  cfg = []; 
  cfg.xlim = [-0.7 1];%do not accept any artifacts within this range %[-.5 2];
  cfg.ylim = [0 150];
  cfg.zlim = [-1 3];
  cfg.colorbar = 'yes'; 

  i=1; 

  artif(VP).artfctdef.summary.trials = [];

  %Save for uploading on Marcos' script
  %save(char(fullfile(['/Volumes/Promise_Pegasus/Alba/WP3_iEEG_data/ruber', '/', ['Patient', subjnum], '/derivatives_clean'],[strcat(freq{1,sets}.label, '_run', num2str(f), subjnum, '.mat')])),'freq');

    for l=1:length(Hfreq_base{VP}.trialinfo)%loop through all trials

      h = figure; 
      set (h, 'Units', 'normalized', 'Position', [0,0,1,1]);
      switch i
          case 1
              label = 'eHit';
      end

   artif(VP).values = Hfreq_base{VP}; 
   artif(VP).values.powspctrm = Hfreq_base{VP}.powspctrm(l,:,:,:);
   artif(VP).values.label = Hfreq_base{VP}.label;

  %%
  cfg.layout = 'biosemi128.lay';
  
  ft_multiplotTFR(cfg,artif(VP).values);

  title(['High frequencies, subject ' num2str(VP)]);

  fprintf(strcat('*****************trial:_',num2str(l)))
  fprintf('\n')
  artifact = 'a';

    while (artifact ~= 'y' && artifact ~= 'n')
        artifact = input('Is this trial an artifact? (y/n): ','s'); %control c stops it 
    end
    if artifact == 'y'
        artif(VP).artfctdef.summary.trials = [artif(VP).artfctdef.summary.trials l];
    elseif artifact == 'n'
        %do nothing
    else
        
        return
    end
    close all
    end
 end

% in case the script crashes because you typed something wrong, you can use
% this:
% artftemp = artif(VP).artfctdef.summary.trials;
% artftemp2 = [artftemp artif(VP).artfctdef.summary.trials];
% artftemp3 = [artftemp2 artif(18).artfctdef.summary.trials];
% 
% artif(VP).artfctdef.summary.trials = artftemp2;

save artif_AHR_freq artif


%%% select and discard trials from dataset
%pre-allocate memory to save time
cleaned = cell(1,length(ref_AHR));
cleaned{1,length(ref_AHR)} = [];
for VP = 1:length(ref_AHR)
    data = ref_AHR{VP};
    cfg = [];
    trialstokeep = 1:length(data.trial);
    trialstokeep([artif(VP).artfctdef.summary.trials]) = [];
    cfg.trials = trialstokeep;
    cleaned{VP} = ft_selectdata(cfg,data);
end

% save this dataset so we can use it for the ERP analysis as well
save cleaned_AHR.mat '-v7.3' cleaned;



%% Now we can do the actual time frequency analysis 
clearvars
load('..\cleaned_AHR.mat');

%%pre-allocate memory to save time
freq_neg_AHR = cell(1,length(cleaned));
freq_neg_AHR{1,length(cleaned)} = [];

freq_ntr_AHR = cell(1,length(cleaned));
freq_ntr_AHR{1,length(cleaned)} = [];

foi = 30:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
tap=k-1;
else
tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; % length of time window in seconds
    cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; 
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'yes'; % if you want to look at single subejct data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);
% 
    cfg=[];
    cfg.avgoverrpt = 'no';
    freq_neg_AHR{VP} = ft_selectdata(cfg,freq_ses11);
    freq_ntr_AHR{VP} = ft_selectdata(cfg,freq_ses12);

    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
end

save('freq_neg_AHR.mat', '-v7.3', 'freq_neg_AHR');
save('freq_ntr_AHR.mat', '-v7.3', 'freq_ntr_AHR');



