%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% % this script runs a timelocked analysis on the EEG data and plots ERP timeseries and differential topographies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR


addpath C:\Users\enyaw\sciebo\AttEmo_Material\
ft_startup


% load cleaned data
clean_AHR = load('cleaned_AHR.mat');
clean_CON = load('cleaned_CON.mat'); 
clean_AHL = load('cleaned_AHL.mat');

%% ERP analysis for all three groups

% rTLR
for VP = 1:length(clean_AHR.cleaned)
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 40; % i am using an additional lpfilter for the ERPs
    cfg.demean = 'yes';
    cfg.baselinewindow = [-0.2 0];
    cleaned_AHR = ft_preprocessing(cfg, clean_AHR.cleaned{VP});
    cfg = [];
    data = cleaned_AHR;
    cfg.trials    = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_AHR{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials    = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_AHR{VP} = ft_timelockanalysis(cfg, data);
    clear cleaned_AHR;
end

%lTLR
for VP = 1:length(clean_AHL.cleaned)
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 40; % i am using an additional lpfilter for the ERPs
    cfg.demean = 'yes';
    cfg.baselinewindow = [-0.2 0];
    cleaned_AHL = ft_preprocessing(cfg, clean_AHL.cleaned{VP});
    cfg = [];
    data = cleaned_AHL;
    cfg.trials    = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_AHL{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials    = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_AHL{VP} = ft_timelockanalysis(cfg, data);
    clear cleaned_AHL;
end

% HC
for VP = 1:length(clean_CON.cleaned)
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 40; % i am using an additional lpfilter for the ERPs
    cfg.demean = 'yes';
    cfg.baselinewindow = [-0.2 0];
    cleaned_CON = ft_preprocessing(cfg, clean_CON.cleaned{VP});
    cfg = [];
    data = cleaned_CON;
    cfg.trials    = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_CON{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials    = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_CON{VP} = ft_timelockanalysis(cfg, data); 
    clear cleaned_CON;
end


%%% grand averaging for plotting

% note that i am excluding some particpants here by omitting some of the
% array entries from the gavg
cfg=[];
cfg.keepindividual='no';
gavg_CON_neg = ft_timelockgrandaverage(cfg,timelock_neg_CON{[1:17 19]});
gavg_CON_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_CON{[1:17 19]});
gavg_AHR_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHR{[1:3 5:8 10:20]});
gavg_AHR_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHR{[1:3 5:8 10:20]});
gavg_AHL_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHL{[1:3 5:14 16:20]});
gavg_AHL_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHL{[1:3 5:14 16:20]});


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PLOTTING
%% topographies
addpath ..\colorschemes; % this you need for the color brewer colormaps: https://www.mathworks.com/matlabcentral/fileexchange/45208-colorbrewer-attractive-and-distinctive-colormaps

% compute differences for differential topographies
cfg = [];
cfg.operation = 'x1-x2';
cfg.parameter = 'avg';
diff_CON = ft_math(cfg, gavg_CON_neg, gavg_CON_ntr);
diff_AHR = ft_math(cfg, gavg_AHR_neg, gavg_AHR_ntr);
diff_AHL = ft_math(cfg, gavg_AHL_neg, gavg_AHL_ntr);

% set the configuration for plotting
% note that this script instantly saves the plots. if you don't want to save
% something, just comment this part out

cfg = [];
cfg.layout = 'biosemi128.lay';
cfg.colorbar = 'no';
cfg.comment = 'no';
cfg.colormap = flipud(colormap(brewermap([],'Spectral')));

%P1
cfg.zlim = [-0.6 0.6]; % colorbar limit
cfg.xlim = [0.09 0.13]; % time-points that will be averaged

ft_topoplotER(cfg, diff_CON)
saveas (gcf, ['topo_p1_con.jpg']);

ft_topoplotER(cfg, diff_AHR)
saveas (gcf, ['topo_p1_ahr.jpg']);

ft_topoplotER(cfg, diff_AHL)
saveas (gcf, ['topo_p1_ahl.jpg']);

% N1
cfg.zlim = [-1 1]; % colorbar limit
cfg.xlim = [0.14 0.2]; % time-points that will be averaged

ft_topoplotER(cfg, diff_CON) 
saveas (gcf, ['topo_n1_con.jpg']);

ft_topoplotER(cfg, diff_AHR)
saveas (gcf, ['topo_n1_ahr.jpg']);

ft_topoplotER(cfg, diff_AHL)
saveas (gcf, ['topo_n1_ahl.jpg']);


% EPN
cfg.xlim = [0.2 0.35]; % time-points that will be averaged

ft_topoplotER(cfg, diff_CON)
saveas (gcf, ['topo_epn_con.jpg']);

ft_topoplotER(cfg, diff_AHR)
saveas (gcf, ['topo_epn_ahr.jpg']);

ft_topoplotER(cfg, diff_AHL)
saveas (gcf, ['topo_epn_ahl.jpg']);


% LPP
cfg.xlim = [0.4 0.8]; % time-points that will be averaged
ft_topoplotER(cfg, diff_CON)
saveas (gcf, ['topopp_con.jpg']);

ft_topoplotER(cfg, diff_AHR)
saveas (gcf, ['topopp_ahr.jpg']);

ft_topoplotER(cfg, diff_AHL)
saveas (gcf, ['topopp_ahl.jpg']);


%% timeseries
% here you have to select the channels first that you want to average to obtain one
% timeseries, then you will also compute the average difference for plotting

% you need the boundedline function for plotting: https://www.mathworks.com/matlabcentral/fileexchange/27485-boundedline-m

addpath ..\UserFunctions % this is where i store the boundedline function
%%% do a grand average and then select the channel clusters that you want
%%% to plot

%%% P1

cfg=[];
cfg.keepindividual='no';
gavg_CON_neg = ft_timelockgrandaverage(cfg,timelock_neg_CON{[1:17 19]});
gavg_CON_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_CON{[1:17 19]});
gavg_AHR_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHR{[1:3 5:8 10:20]});
gavg_AHR_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHR{[1:3 5:8 10:20]});
gavg_AHL_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHL{[1:3 5:14 16:20]});
gavg_AHL_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHL{[1:3 5:14 16:20]});


cfg = [];
cfg.channel = {'D31', 'D32', 'A10', 'A11', 'A12','B10', 'B11',  'B7', 'B8', 'B9'}; 
cfg.avgoverchan = 'yes';
gavg_CON_neg = ft_selectdata(cfg,gavg_CON_neg);
gavg_CON_ntr = ft_selectdata(cfg,gavg_CON_ntr);
gavg_AHR_neg = ft_selectdata(cfg,gavg_AHR_neg);
gavg_AHR_ntr = ft_selectdata(cfg,gavg_AHR_ntr);
gavg_AHL_neg = ft_selectdata(cfg,gavg_AHL_neg);
gavg_AHL_ntr = ft_selectdata(cfg,gavg_AHL_ntr);

%%% now do the actual plotting
% you will first select what to plot (avg and se) and subsequently plot
% those vectors

time = gavg_CON_neg.time; % this is for the x axis
         

avg_neg_con = gavg_CON_neg.avg;

avg_ntr_con = gavg_CON_ntr.avg;

avg_neg_ahr = gavg_AHR_neg.avg;

avg_ntr_ahr = gavg_AHR_ntr.avg;

avg_neg_ahl = gavg_AHL_neg.avg;

avg_ntr_ahl = gavg_AHL_ntr.avg;

figure('Color', [1 1 1], 'Renderer', 'painters', 'Position', [10 10 500 500])

boundedline(time,avg_neg_con,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_ntr_con,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_neg_ahr,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5, '--');
boundedline(time,avg_ntr_ahr,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5,'--');
boundedline(time,avg_neg_ahl,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 3.5, ':');
boundedline(time,avg_ntr_ahl,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 3.5,':');
grid on, set(hline(0,'k')), 
set(gca, 'FontName', 'Arial', 'FontSize',24, 'linewidth', 3),
xticks([0 0.1 ]),
yticks([0 2 4]),
axis([-0.03 0.15 -0.5 4.5]),
line([0 0],[-5 10],'Color','black');   
p1 = patch([0.09 0.13 0.13 0.09], [min(ylim) min(ylim) [1 1]*max(ylim)], [0 0 0], 'FaceAlpha',0.1, 'EdgeColor','none');
p1.Annotation.LegendInformation.IconDisplayStyle = 'off';

xlabel('Time (s)','FontSize', 30)
ylabel('Amplitude (�V)','FontSize', 30)

saveas (gcf, ['erp_p1.jpg']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% N1/EPN

cfg=[];
cfg.keepindividual='no';
gavg_CON_neg = ft_timelockgrandaverage(cfg,timelock_neg_CON{[1:17 19]});
gavg_CON_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_CON{[1:17 19]});
gavg_AHR_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHR{[1:3 5:8 10:20]});
gavg_AHR_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHR{[1:3 5:8 10:20]});
gavg_AHL_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHL{[1:3 5:14 16:20]});
gavg_AHL_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHL{[1:3 5:14 16:20]});


cfg = [];
cfg.channel = {'D31', 'D32', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A26', 'A27', 'A28', 'B7', 'B8', 'B9', 'B10', 'B11'};  
cfg.avgoverchan = 'yes';
gavg_CON_neg = ft_selectdata(cfg,gavg_CON_neg);
gavg_CON_ntr = ft_selectdata(cfg,gavg_CON_ntr);
gavg_AHR_neg = ft_selectdata(cfg,gavg_AHR_neg);
gavg_AHR_ntr = ft_selectdata(cfg,gavg_AHR_ntr);
gavg_AHL_neg = ft_selectdata(cfg,gavg_AHL_neg);
gavg_AHL_ntr = ft_selectdata(cfg,gavg_AHL_ntr);

%%%

time = gavg_CON_neg.time;
         
avg_neg_con = gavg_CON_neg.avg;

avg_ntr_con = gavg_CON_ntr.avg;

avg_neg_ahr = gavg_AHR_neg.avg;

avg_ntr_ahr = gavg_AHR_ntr.avg;

avg_neg_ahl = gavg_AHL_neg.avg;

avg_ntr_ahl = gavg_AHL_ntr.avg;

% because N1 and EPN use the same clusters, you can use the same avg and se
% for plotting

figure('Color', [1 1 1], 'Renderer', 'painters', 'Position', [10 10 500 500])

boundedline(time,avg_neg_con,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_ntr_con,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_neg_ahr,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5, '--');
boundedline(time,avg_ntr_ahr,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5,'--');
boundedline(time,avg_neg_ahl,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 3.5, ':');
boundedline(time,avg_ntr_ahl,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 3.5,':');
grid on, set(hline(0,'k')), 
set(gca, 'FontName', 'Arial', 'FontSize',24, 'linewidth', 3),
xticks([-0.2 0 0.1 0.2 0.3 0.4]),
yticks([-4 -2 0 2 4]),
axis([-0.03 0.22 -4.5 4.5]),
line([0 0],[-5 10],'Color','black');   
p1 = patch([0.14 0.2 0.2 0.14], [min(ylim) min(ylim) [1 1]*max(ylim)], [0 0 0], 'FaceAlpha',0.1, 'EdgeColor','none');
p1.Annotation.LegendInformation.IconDisplayStyle = 'off';

% Add shared title and axis labels
xlabel('Time (s)','FontSize', 30)
ylabel('Amplitude (�V)','FontSize', 30)

saveas (gcf, ['erp_n1.jpg']);


%%%%%% EPN
figure('Color', [1 1 1], 'Renderer', 'painters', 'Position', [10 10 500 500])

boundedline(time,avg_neg_con,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_ntr_con,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_neg_ahr,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5, '--');
boundedline(time,avg_ntr_ahr,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5,'--');
boundedline(time,avg_neg_ahl,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 3.5, ':');
boundedline(time,avg_ntr_ahl,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 3.5,':');
grid on, set(hline(0,'k')), 
set(gca, 'FontName', 'Arial', 'FontSize',24, 'linewidth', 3),
xticks([-0.2 0 0.1 0.2 0.3 0.4]),
yticks([-4 -2 0 2 4 6 8]),
axis([-0.03 0.4 -4 8.5]),
line([0 0],[-5 10],'Color','black');   
p1 = patch([0.2 0.35 0.35 0.2], [min(ylim) min(ylim) [1 1]*max(ylim)], [0 0 0], 'FaceAlpha',0.1, 'EdgeColor','none');
p1.Annotation.LegendInformation.IconDisplayStyle = 'off';

% Add shared title and axis labels
xlabel('Time (s)','FontSize', 30)
ylabel('Amplitude (�V)','FontSize', 30)

saveas (gcf, ['erp_epn.jpg']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% LPP

cfg=[];
cfg.keepindividual='no';
gavg_CON_neg = ft_timelockgrandaverage(cfg,timelock_neg_CON{[1:17 19]});
gavg_CON_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_CON{[1:17 19]});
gavg_AHR_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHR{[1:3 5:8 10:20]});
gavg_AHR_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHR{[1:3 5:8 10:20]});
gavg_AHL_neg = ft_timelockgrandaverage(cfg,timelock_neg_AHL{[1:3 5:14 16:20]});
gavg_AHL_ntr = ft_timelockgrandaverage(cfg,timelock_ntr_AHL{[1:3 5:14 16:20]});


cfg = [];
cfg.channel = {'D1', 'C1', 'A1', 'D15', 'A2', 'B1', 'D14', 'B20', 'D16', 'A3', 'B2', 'D17', 'A4', 'B19', 'A6', 'A5', 'A19', 'A32', 'B3', 'B4', 'A31', 'A20', 'A18', 'A7'};
cfg.avgoverchan = 'yes';
gavg_CON_neg = ft_selectdata(cfg,gavg_CON_neg);
gavg_CON_ntr = ft_selectdata(cfg,gavg_CON_ntr);
gavg_AHR_neg = ft_selectdata(cfg,gavg_AHR_neg);
gavg_AHR_ntr = ft_selectdata(cfg,gavg_AHR_ntr);
gavg_AHL_neg = ft_selectdata(cfg,gavg_AHL_neg);
gavg_AHL_ntr = ft_selectdata(cfg,gavg_AHL_ntr);

time = gavg_CON_neg.time;

% 
avg_neg_con = gavg_CON_neg.avg;

avg_ntr_con = gavg_CON_ntr.avg;

avg_neg_ahr = gavg_AHR_neg.avg;

avg_ntr_ahr = gavg_AHR_ntr.avg;

avg_neg_ahl = gavg_AHL_neg.avg;

avg_ntr_ahl = gavg_AHL_ntr.avg;

%%%%%% plot single conditions in three subplots
figure('Color', [1 1 1], 'Renderer', 'painters', 'Position', [10 10 500 500])


boundedline(time,avg_neg_con,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_ntr_con,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5);
boundedline(time,avg_neg_ahr,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 2.5, '--');
boundedline(time,avg_ntr_ahr,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 2.5,'--');
boundedline(time,avg_neg_ahl,0, 'cmap',[1 0 0],'alpha','transparency',0.1, 'linewidth', 3.5, ':');
boundedline(time,avg_ntr_ahl,0, 'cmap',[0.3 0.3 0.3],'alpha','transparency',0.1, 'linewidth', 3.5,':');
grid on, set(hline(0,'k')), 
set(gca, 'FontName', 'Arial', 'FontSize',24, 'linewidth', 3),
xticks([0 0.2 0.4 0.6 0.8]),
yticks([-1 0 1 2]),
axis([-0.03 0.82 -1 2.5]),
line([0 0],[-5 10],'Color','black');   
p1 = patch([0.4 0.8 0.8 0.4], [min(ylim) min(ylim) [1 1]*max(ylim)], [0 0 0], 'FaceAlpha',0.1, 'EdgeColor','none');
p1.Annotation.LegendInformation.IconDisplayStyle = 'off';


xlabel('Time (s)','FontSize', 30)
ylabel('Amplitude (�V)','FontSize', 30)


saveas (gcf, ['erp_lpp.jpg']);
   


%% optional: make layout plot for marking plotting channel clusters
cfg = [];
cfg.layout = 'biosemi128.lay';
layout128 = ft_prepareayout(cfg);

figure('Color', [1 1 1], 'Renderer', 'painters')
ft_plotayout(layout128, 'box', 'no', 'label', 'no', 'pointcolor', 'k', 'pointsymbol', 'o', 'pointsize', 20);

