%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% % this script exports the ERP data to a text file that you can read in with
% R to perform statistical analyses. the code exports single-trials data,
% that's why it is important to specify the option "cfg.keeptrials = yes"
% in the timelock analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR

ft_startup

clean_AHR = load('..\cleaned_AHR.mat');
clean_CON = load('..\cleaned_CON.mat'); %or reject until i have cleaned the tf data again
clean_AHL = load('..\cleaned_AHL.mat');


%% ERP analysis
% i am not pre-allocating memory here but you can do this if you want the
% script to run a bit more efficiently

% rTLR
for VP = 1:length(clean_AHR.cleaned)
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    cfg.demean          = 'yes';
    cfg.baselinewindow  = [-0.2 0];
    cleaned_AHR         = ft_preprocessing(cfg, clean_AHR.cleaned{VP});
    cfg                 = [];
    data                = cleaned_AHR;
    cfg.keeptrials      = 'yes';
    cfg.trials          = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_AHR{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials          = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_AHR{VP} = ft_timelockanalysis(cfg, data);
    clear cleaned_AHR;
end

% lTLR
for VP = 1:length(clean_AHL.cleaned)
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    cfg.demean          = 'yes';
    cfg.baselinewindow  = [-0.2 0];
    cleaned_AHL         = ft_preprocessing(cfg, clean_AHL.cleaned{VP});
    cfg                 = [];
    data                = cleaned_AHL;
    cfg.keeptrials      = 'yes';
    cfg.trials          = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_AHL{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials          = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_AHL{VP} = ft_timelockanalysis(cfg, data);
    clear cleaned_AHL;
end

% HC
for VP = 1:length(clean_CON.cleaned)
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    cfg.demean          = 'yes';
    cfg.baselinewindow  = [-0.2 0];
    cleaned_CON         = ft_preprocessing(cfg, clean_CON.cleaned{VP});
    cfg                 = [];
    data                = cleaned_CON;
    cfg.keeptrials      = 'yes';
    cfg.trials          = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_CON{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials          = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_CON{VP} = ft_timelockanalysis(cfg, data); 
    clear cleaned_CON;
end



%% EXPORT including info about trial, subject, hemipshere, group and emotion

cd ..\single_trials

% define time windows and channels for the components beforehand
P1_time = [0.09 0.13];
N1_time = [0.14 0.2];
EPN_time = [0.2 0.35];
LPP_time = [0.4 0.8];

P1_chan_left = {'D31', 'D32', 'A10', 'A11', 'A12'};
P1_chan_right = {'B7', 'B8', 'B9', 'B10', 'B11'};
N1_chan_left = {'D31', 'D32', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15'}; 
EPN_chan_left = {'D31', 'D32', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15'}; 
N1_chan_right = {'A26', 'A27', 'A28', 'B7', 'B8', 'B9', 'B10', 'B11'}; 
EPN_chan_right = {'A26', 'A27', 'A28', 'B7', 'B8', 'B9', 'B10', 'B11'}; 
LPP_chan = {'D1', 'C1', 'A1', 'D15', 'A2', 'B1', 'D14', 'B20', 'D16', 'A3', 'B2', 'D17', 'A4', 'B19', 'A6', 'A5', 'A19', 'A32', 'B3', 'B4', 'A31', 'A20', 'A18', 'A7'};

%% P1

% HC
idx = 0;
P1_left = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg             = [];
    cfg.channel     = P1_chan_left;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_CON{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo % 1 = negative, 2 = neutral
    P1_neg(1:length(data_neg.trial),4) = 1; % group % 1 = hc, 2 = lTLR, 3 = rTLR
    P1_neg(1:length(data_neg.trial),5) = 1; % hemi % 1 = left, 2 = right
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo % 1 = negative, 2 = neutral
    P1_ntr(1:length(data_ntr.trial),4) = 1; % group % 1 = hc, 2 = lTLR, 3 = rTLR
    P1_ntr(1:length(data_ntr.trial),5) = 1; % hemi % 1 = left, 2 = right
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_left = [P1_left; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

idx = 0;
P1_right = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg                     = [];
    cfg.channel             = P1_chan_right;
    cfg.latency             = P1_time;
    cfg.avgoverchan         = 'yes';
    cfg.avgovertime         = 'yes';
    data_neg                = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr                = ft_selectdata(cfg, timelock_ntr_CON{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 1; % group
    P1_neg(1:length(data_neg.trial),5) = 2; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    P1_ntr(1:length(data_ntr.trial),4) = 1; % group 
    P1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_right = [P1_right; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

P1_con = array2table(num2cell([P1_left; P1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% lTLR
idx = 0;
P1_left = [];
for VP = [1:3 5:14 16:20] 
    idx = idx +1;
 
    cfg             = [];
    cfg.channel     = P1_chan_left;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+36; % subjID % i am adding a random number > 21 to avoid having the same subj IDs
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 2; % group
    P1_neg(1:length(data_neg.trial),5) = 1; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    P1_ntr(1:length(data_ntr.trial),4) = 2; % group
    P1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_left = [P1_left; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

idx = 0;
P1_right = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;
   
    cfg             = [];
    cfg.channel     = P1_chan_right;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 2; % group
    P1_neg(1:length(data_neg.trial),5) = 2; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    P1_ntr(1:length(data_ntr.trial),4) = 2; % group 
    P1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_right = [P1_right; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

P1_ahl = array2table(num2cell([P1_left; P1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% rTLR
idx = 0;
P1_left = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = P1_chan_left;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 3; % group
    P1_neg(1:length(data_neg.trial),5) = 1; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    P1_ntr(1:length(data_ntr.trial),4) = 3; % group
    P1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_left = [P1_left; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

idx = 0;
P1_right = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = P1_chan_right;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 3; % group
    P1_neg(1:length(data_neg.trial),5) = 2; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    P1_ntr(1:length(data_ntr.trial),4) = 3; % group 
    P1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_right = [P1_right; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

P1_ahr = array2table(num2cell([P1_left; P1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});

P1 = [P1_con; P1_ahl; P1_ahr];

writetable(P1, 'P1_singletrials.csv')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% N1 

% HC
idx = 0;
N1_left = [];
for VP = [1:17 19] % con
    idx = idx +1;
    
    cfg             = [];
    cfg.channel     = N1_chan_left;
    cfg.latency     = N1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_CON{VP});

    N1_neg(:,3) = data_neg.trial;
    N1_neg(1:length(data_neg.trial),1) = VP; % subjID
    N1_neg(1:length(data_neg.trial),2) = 1; % emo
    N1_neg(1:length(data_neg.trial),4) = 1; % group
    N1_neg(1:length(data_neg.trial),5) = 1; % hemi
    N1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    N1_ntr(:,3) = data_ntr.trial;
    N1_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    N1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    N1_ntr(1:length(data_ntr.trial),4) = 1; % group
    N1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    N1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    N1_left = [N1_left; N1_neg; N1_ntr];

    clear N1_neg
    clear N1_ntr
end

idx = 0;
N1_right = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg             = [];
    cfg.channel     = N1_chan_right;
    cfg.latency     = N1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_CON{VP});

    N1_neg(:,3) = data_neg.trial;
    N1_neg(1:length(data_neg.trial),1) = VP; % subjID
    N1_neg(1:length(data_neg.trial),2) = 1; % emo
    N1_neg(1:length(data_neg.trial),4) = 1; % group
    N1_neg(1:length(data_neg.trial),5) = 2; % hemi
    N1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    N1_ntr(:,3) = data_ntr.trial;
    N1_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    N1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    N1_ntr(1:length(data_ntr.trial),4) = 1; % group 
    N1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    N1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    N1_right = [N1_right; N1_neg; N1_ntr];

    clear N1_neg
    clear N1_ntr
end

N1_con = array2table(num2cell([N1_left; N1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% lTLR
idx = 0;
N1_left = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;

    cfg             = [];
    cfg.channel     = N1_chan_left;
    cfg.latency     = N1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    N1_neg(:,3) = data_neg.trial;
    N1_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    N1_neg(1:length(data_neg.trial),2) = 1; % emo
    N1_neg(1:length(data_neg.trial),4) = 2; % group
    N1_neg(1:length(data_neg.trial),5) = 1; % hemi
    N1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    N1_ntr(:,3) = data_ntr.trial;
    N1_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    N1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    N1_ntr(1:length(data_ntr.trial),4) = 2; % group
    N1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    N1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    N1_left = [N1_left; N1_neg; N1_ntr];

    clear N1_neg
    clear N1_ntr
end

idx = 0;
N1_right = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;

    cfg             = [];
    cfg.channel     = N1_chan_right;
    cfg.latency     = N1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    N1_neg(:,3) = data_neg.trial;
    N1_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    N1_neg(1:length(data_neg.trial),2) = 1; % emo
    N1_neg(1:length(data_neg.trial),4) = 2; % group
    N1_neg(1:length(data_neg.trial),5) = 2; % hemi
    N1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    N1_ntr(:,3) = data_ntr.trial;
    N1_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    N1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    N1_ntr(1:length(data_ntr.trial),4) = 2; % group 
    N1_ntr(1:length(data_ntr.trial),5) = 2; % hemi
    N1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    N1_right = [N1_right; N1_neg; N1_ntr];

    clear N1_neg
    clear N1_ntr
end

N1_ahl = array2table(num2cell([N1_left; N1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% rTLR
idx = 0;
N1_left = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = N1_chan_left;
    cfg.latency     = N1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    N1_neg(:,3) = data_neg.trial;
    N1_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    N1_neg(1:length(data_neg.trial),2) = 1; % emo
    N1_neg(1:length(data_neg.trial),4) = 3; % group
    N1_neg(1:length(data_neg.trial),5) = 1; % hemi
    N1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    N1_ntr(:,3) = data_ntr.trial;
    N1_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    N1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    N1_ntr(1:length(data_ntr.trial),4) = 3; % group
    N1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    N1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    N1_left = [N1_left; N1_neg; N1_ntr];

    clear N1_neg
    clear N1_ntr
end

idx = 0;
N1_right = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg = [];
    cfg.channel = N1_chan_right;
    cfg.latency = N1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    N1_neg(:,3) = data_neg.trial;
    N1_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    N1_neg(1:length(data_neg.trial),2) = 1; % emo
    N1_neg(1:length(data_neg.trial),4) = 3; % group
    N1_neg(1:length(data_neg.trial),5) = 2; % hemi
    N1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    N1_ntr(:,3) = data_ntr.trial;
    N1_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    N1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    N1_ntr(1:length(data_ntr.trial),4) = 3; % group 
    N1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    N1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    N1_right = [N1_right; N1_neg; N1_ntr];

    clear N1_neg
    clear N1_ntr
end

N1_ahr = array2table(num2cell([N1_left; N1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'}):


N1 = [N1_con; N1_ahl; N1_ahr];

writetable(N1, 'N1_singletrials.csv')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EPN 

% HC
idx = 0;
EPN_left = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg             = [];
    cfg.channel     = EPN_chan_left;
    cfg.latency     = EPN_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_CON{VP});

    EPN_neg(:,3) = data_neg.trial;
    EPN_neg(1:length(data_neg.trial),1) = VP; % subjID
    EPN_neg(1:length(data_neg.trial),2) = 1; % emo
    EPN_neg(1:length(data_neg.trial),4) = 1; % group
    EPN_neg(1:length(data_neg.trial),5) = 1; % hemi
    EPN_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    EPN_ntr(:,3) = data_ntr.trial;
    EPN_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    EPN_ntr(1:length(data_ntr.trial),2) = 2; % emo
    EPN_ntr(1:length(data_ntr.trial),4) = 1; % group
    EPN_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    EPN_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    EPN_left = [EPN_left; EPN_neg; EPN_ntr];

    clear EPN_neg
    clear EPN_ntr
end

idx = 0;
EPN_right = [];
for VP = [1:17 19] % con
    idx = idx +1;
  
    cfg             = [];
    cfg.channel     = EPN_chan_right;
    cfg.latency     = EPN_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_CON{VP});

    EPN_neg(:,3) = data_neg.trial;
    EPN_neg(1:length(data_neg.trial),1) = VP; % subjID
    EPN_neg(1:length(data_neg.trial),2) = 1; % emo
    EPN_neg(1:length(data_neg.trial),4) = 1; % group
    EPN_neg(1:length(data_neg.trial),5) = 2; % hemi
    EPN_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    EPN_ntr(:,3) = data_ntr.trial;
    EPN_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    EPN_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    EPN_ntr(1:length(data_ntr.trial),4) = 1; % group 
    EPN_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    EPN_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    EPN_right = [EPN_right; EPN_neg; EPN_ntr];

    clear EPN_neg
    clear EPN_ntr
end

EPN_con = array2table(num2cell([EPN_left; EPN_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% lTLR
idx = 0;
EPN_left = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;

    cfg             = [];
    cfg.channel     = EPN_chan_left;
    cfg.latency     = EPN_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    EPN_neg(:,3) = data_neg.trial;
    EPN_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    EPN_neg(1:length(data_neg.trial),2) = 1; % emo
    EPN_neg(1:length(data_neg.trial),4) = 2; % group
    EPN_neg(1:length(data_neg.trial),5) = 1; % hemi
    EPN_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    EPN_ntr(:,3) = data_ntr.trial;
    EPN_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    EPN_ntr(1:length(data_ntr.trial),2) = 2; % emo
    EPN_ntr(1:length(data_ntr.trial),4) = 2; % group
    EPN_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    EPN_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    EPN_left = [EPN_left; EPN_neg; EPN_ntr];

    clear EPN_neg
    clear EPN_ntr
end

idx = 0;
EPN_right = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;

    cfg             = [];
    cfg.channel     = EPN_chan_right;
    cfg.latency     = EPN_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    EPN_neg(:,3) = data_neg.trial;
    EPN_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    EPN_neg(1:length(data_neg.trial),2) = 1; % emo
    EPN_neg(1:length(data_neg.trial),4) = 2; % group
    EPN_neg(1:length(data_neg.trial),5) = 2; % hemi
    EPN_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    EPN_ntr(:,3) = data_ntr.trial;
    EPN_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    EPN_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    EPN_ntr(1:length(data_ntr.trial),4) = 2; % group 
    EPN_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    EPN_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    EPN_right = [EPN_right; EPN_neg; EPN_ntr];

    clear EPN_neg
    clear EPN_ntr
end

EPN_ahl = array2table(num2cell([EPN_left; EPN_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% rTLR
idx = 0;
EPN_left = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = EPN_chan_left;
    cfg.latency     = EPN_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    EPN_neg(:,3) = data_neg.trial;
    EPN_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    EPN_neg(1:length(data_neg.trial),2) = 1; % emo
    EPN_neg(1:length(data_neg.trial),4) = 3; % group
    EPN_neg(1:length(data_neg.trial),5) = 1; % hemi
    EPN_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    EPN_ntr(:,3) = data_ntr.trial;
    EPN_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    EPN_ntr(1:length(data_ntr.trial),2) = 2; % emo
    EPN_ntr(1:length(data_ntr.trial),4) = 3; % group
    EPN_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    EPN_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    EPN_left = [EPN_left; EPN_neg; EPN_ntr];

    clear EPN_neg
    clear EPN_ntr
end

idx = 0;
EPN_right = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = EPN_chan_right;
    cfg.latency     = EPN_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    EPN_neg(:,3) = data_neg.trial;
    EPN_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    EPN_neg(1:length(data_neg.trial),2) = 1; % emo
    EPN_neg(1:length(data_neg.trial),4) = 3; % group
    EPN_neg(1:length(data_neg.trial),5) = 2; % hemi
    EPN_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    EPN_ntr(:,3) = data_ntr.trial;
    EPN_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    EPN_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    EPN_ntr(1:length(data_ntr.trial),4) = 3; % group 
    EPN_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    EPN_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    EPN_right = [EPN_right; EPN_neg; EPN_ntr];

    clear EPN_neg
    clear EPN_ntr
end

EPN_ahr = array2table(num2cell([EPN_left; EPN_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


EPN = [EPN_con; EPN_ahl; EPN_ahr];

writetable(EPN, 'EPN_singletrials.csv')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% LPP 

% HC
idx = 0;
LPP = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg             = [];
    cfg.channel     = LPP_chan;
    cfg.latency     = LPP_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg    = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr    = ft_selectdata(cfg, timelock_ntr_CON{VP});

    LPP_neg(:,3) = data_neg.trial;
    LPP_neg(1:length(data_neg.trial),1) = VP; % subjID
    LPP_neg(1:length(data_neg.trial),2) = 1; % emo
    LPP_neg(1:length(data_neg.trial),4) = 1; % group
    LPP_neg(1:length(data_neg.trial),5) = 1; % hemi
    LPP_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    LPP_ntr(:,3) = data_ntr.trial;
    LPP_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    LPP_ntr(1:length(data_ntr.trial),2) = 2; % emo
    LPP_ntr(1:length(data_ntr.trial),4) = 1; % group
    LPP_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    LPP_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    LPP = [LPP; LPP_neg; LPP_ntr];

    clear LPP_neg
    clear LPP_ntr
end

LPP_con = array2table(num2cell(LPP), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% lTLR
idx = 0;
LPP = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;

    cfg             = [];
    cfg.channel     = LPP_chan;
    cfg.latency     = LPP_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    LPP_neg(:,3) = data_neg.trial;
    LPP_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    LPP_neg(1:length(data_neg.trial),2) = 1; % emo
    LPP_neg(1:length(data_neg.trial),4) = 2; % group
    LPP_neg(1:length(data_neg.trial),5) = 1; % hemi
    LPP_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    LPP_ntr(:,3) = data_ntr.trial;
    LPP_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    LPP_ntr(1:length(data_ntr.trial),2) = 2; % emo (1 = neg)
    LPP_ntr(1:length(data_ntr.trial),4) = 2; % group (1 = con, 2 = ahr, 3 = ahl)
    LPP_ntr(1:length(data_ntr.trial),5) = 1; % hemi (1 = left)
    LPP_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    LPP = [LPP; LPP_neg; LPP_ntr];

    clear LPP_neg
    clear LPP_ntr
end

LPP_ahl = array2table(num2cell(LPP), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% rTLR
idx = 0;
LPP = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = LPP_chan;
    cfg.latency     = LPP_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    LPP_neg(:,3) = data_neg.trial;
    LPP_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    LPP_neg(1:length(data_neg.trial),2) = 1; % emo
    LPP_neg(1:length(data_neg.trial),4) = 3; % group
    LPP_neg(1:length(data_neg.trial),5) = 1; % hemi
    LPP_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    LPP_ntr(:,3) = data_ntr.trial;
    LPP_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    LPP_ntr(1:length(data_ntr.trial),2) = 2; % emo
    LPP_ntr(1:length(data_ntr.trial),4) = 3; % group
    LPP_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    LPP_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    LPP = [LPP; LPP_neg; LPP_ntr];

    clear LPP_neg
    clear LPP_ntr
end


LPP_ahr = array2table(num2cell(LPP), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


LPP_total = [LPP_con; LPP_ahl; LPP_ahr];

writetable(LPP_total, 'LPP_singletrials.csv')

