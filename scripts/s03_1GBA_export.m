%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% % this script exports the time frequency data to a text file that you can perform permutation analyses. the code exports single-trials data,
% that's why it is important to specify the option "cfg.keeptrials = yes"
% in the timelock analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the permutations are later calculated in matlab. this is faster than R because of all the loops

clearvars
ft_startup

% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR


%% make dataset
% it is important that you remeber the dimensions of the data and the order
% of factors so that you don't mix this up in the stats later

% we will extract the data that we are interested in and export it

% first we delete columns of subjects you don't want to keep (to make
% it less error prone)

% then put them all into a big dataframe (for each channel separately to make
% computing easier)

% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR

% HC
load('f_neg_CON.mat');
f_neg_CON(:,20) = [];
f_neg_CON(:,18) = [];

data = f_neg_CON;
clear f_neg_CON;

numchan = length(data{1}(1,:,1,1));
numtime = length(data{1}(:,1,1,1));
numfreq = length(data{1}(1,1,:,1));
numsubj = length(data);

for c = 1:numchan
 r = 0;
 big_data_CON_neg = zeros(numsubj*160*numfreq*numtime,8);
   for VP = 1:numsubj
        for tr = 1:length(data{VP}(:,1,1,1))
            for f = 1:numfreq
                for t = 1:numtime
                    r = r + 1;
                    big_data_CON_neg(r,1) = (data{VP}(tr,c,f,t));
                    big_data_CON_neg(r,2) = VP; % subject ID
                    big_data_CON_neg(r,3) = tr; % trial number
                    big_data_CON_neg(r,4) = c; % channel number
                    big_data_CON_neg(r,5) = f; % freq bin
                    big_data_CON_neg(r,6) = t; % timepoint
                    big_data_CON_neg(r,7) = 1; % emo(1 = neg)
                    big_data_CON_neg(r,8) = 1; % group (1 = con, 2 = ahl, 3 = ahr)
                end
            end 
        end
   end
   
   % SAVE DATA
    big_data_CON_neg2 =array2table(big_data_CON_neg, 'VariableNames',{'value','subj', 'trial',...
    'chan', 'freq', 'timepoint', 'emo', 'group'});
    big_data_CON_neg2 = big_data_CON_neg2(~(big_data_CON_neg2.group == 0 ),:); 

    writetable(big_data_CON_neg2, strcat('big_data_CON_neg_c',num2str(c),'.csv'))
    clear big_data_CON_neg
    clear big_data_CON_neg2
end
clearvars



load('f_ntr_CON.mat');
f_ntr_CON(:,20) = [];
f_ntr_CON(:,18) = [];

data = f_ntr_CON;
clear f_ntr_CON;

for c = 1:numchan
 r = 0;
 big_data_CON_ntr = zeros(numsubj*160*numfreq*numtime,8);
   for VP = 1:numsubj
        for tr = 1:length(data{VP}(:,1,1,1))
            for f = 1:numfreq
                for t = 1:numtime
                    r = r + 1;
                    big_data_CON_ntr(r,1) = (data{VP}(tr,c,f,t));
                    big_data_CON_ntr(r,2) = VP;
                    big_data_CON_ntr(r,3) = tr;
                    big_data_CON_ntr(r,4) = c;
                    big_data_CON_ntr(r,5) = f;
                    big_data_CON_ntr(r,6) = t;
                    big_data_CON_ntr(r,7) = 2; 
                    big_data_CON_ntr(r,8) = 1; 
                end
            end 
        end
   end
   
   % SAVE DATA
    big_data_CON_ntr2 =array2table(big_data_CON_ntr, 'VariableNames',{'value','subj', 'trial',...
    'chan', 'freq', 'timepoint', 'emo', 'group'});
    big_data_CON_ntr2 = big_data_CON_ntr2(~(big_data_CON_ntr2.group == 0 ),:); 

    writetable(big_data_CON_ntr2, strcat('big_data_CON_ntr_c',num2str(c),'.csv'))
    clear big_data_CON_ntr
    clear big_data_CON_ntr2
end
clearvars

%% lTLR
load('f_neg_AHL.mat');
f_neg_AHL(:,15) = [];
f_neg_AHL(:,4) = [];

data = f_neg_AHL;
clear f_neg_AHL;

for c = 1:numchan
    %pre-allocate memory!!!
    big_data_AHL_neg = zeros(numsubj*160*numfreq*numtime,8);

    r = 0;
    for VP =1:numsubj
        for tr = 1:length(data{VP}(:,1,1,1))
            for f = 1:numfreq
                for t = 1:numtime
                    r = r + 1;
                    big_data_AHL_neg(r,1) = (data{VP}(tr,c,f,t));
                    big_data_AHL_neg(r,2) = VP+36;
                    big_data_AHL_neg(r,3) = tr;
                    big_data_AHL_neg(r,4) = c;
                    big_data_AHL_neg(r,5) = f;
                    big_data_AHL_neg(r,6) = t;
                    big_data_AHL_neg(r,7) = 1; %neg
                    big_data_AHL_neg(r,8) = 2; 
                end
            end 
        end
    end
    
    % SAVE DATA
    big_data_AHL_neg2=array2table(big_data_AHL_neg, 'VariableNames',{'value','subj', 'trial',...
    'chan', 'freq', 'timepoint', 'emo', 'group'});
    
    big_data_AHL_neg2 = big_data_AHL_neg2(~(big_data_AHL_neg2.group == 0 ),:);     

    writetable(big_data_AHL_neg2, strcat('big_data_AHL_neg_c',num2str(c),'.csv'))
    clear big_data_AHL_neg
    clear big_data_AHL_neg2
end
clearvars


load('f_ntr_AHL.mat');
f_ntr_AHL(:,15) = [];
f_ntr_AHL(:,4) = [];

data = f_ntr_AHL;
clear f_ntr_AHL;

for c = 1:numchan
    %pre-allocate for speed!!!
    big_data_AHL_ntr = zeros(numsubj*151*numfreq*numtime,8);
    r = 0;
    for VP = 1:numsubj
     trial = length(data{VP}(:,1,1,1));
        for tr = 1:trial
            for f = 1:numfreq
                for t = 1:numtime
                    r = r + 1;
                    big_data_AHL_ntr(r,1) = (data{VP}(tr,c,f,t));
                    big_data_AHL_ntr(r,2) = VP+36;
                    big_data_AHL_ntr(r,3) = tr;
                    big_data_AHL_ntr(r,4) = c;
                    big_data_AHL_ntr(r,5) = f;
                    big_data_AHL_ntr(r,6) = t;
                    big_data_AHL_ntr(r,7) = 2; %ntr
                    big_data_AHL_ntr(r,8) = 2; 
                end
            end 
        end
    end
    
    % SAVE DATA
    big_data_AHL_ntr2=array2table(big_data_AHL_ntr, 'VariableNames',{'value','subj', 'trial',...
    'chan', 'freq', 'timepoint', 'emo', 'group'});
    big_data_AHL_ntr2 = big_data_AHL_ntr2(~(big_data_AHL_ntr2.group == 0 ),:); 

    writetable(big_data_AHL_ntr2, strcat('big_data_AHL_ntr_c',num2str(c),'.csv'))
    clear big_data_AHL_ntr
    clear big_data_AHL_ntr2
end
clearvars



%% rTLR
load('f_neg_AHR.mat');
f_neg_AHR(:,9) = [];
f_neg_AHR(:,4) = [];

data = f_neg_AHR;
clear f_neg_AHR;

for c = 1:numchan
    big_data_AHR_neg = zeros(numsubj*160*numfreq*numtime,8);
    r = 0;
    for VP = 1:numsubj
     trial = length(data{VP}(:,1,1,1));
        for tr = 1:trial
            for f = 1:numfreq
                for t = 1:numtime
                    r = r + 1;
                    big_data_AHR_neg(r,1) = (data{VP}(tr,c,f,t));
                    big_data_AHR_neg(r,2) = VP+60;
                    big_data_AHR_neg(r,3) = tr;
                    big_data_AHR_neg(r,4) = c;
                    big_data_AHR_neg(r,5) = f;
                    big_data_AHR_neg(r,6) = t;
                    big_data_AHR_neg(r,7) = 1; %neg
                    big_data_AHR_neg(r,8) = 3; 
                end
            end 
        end
    end
    % SAVE DATA
    big_data_AHR_neg2=array2table(big_data_AHR_neg, 'VariableNames',{'value','subj', 'trial',...
    'chan', 'freq', 'timepoint', 'emo', 'group'});
    big_data_AHR_neg2 = big_data_AHR_neg2(~(big_data_AHR_neg2.group == 0 ),:); 

    writetable(big_data_AHR_neg2, strcat('big_data_AHR_neg_c',num2str(c),'.csv'))
    clear big_data_AHR_neg
    clear big_data_AHR_neg2
end
clearvars



load('f_ntr_AHR.mat');
f_ntr_AHR(:,9) = [];
f_ntr_AHR(:,4) = [];

data = f_ntr_AHR;
clear f_ntr_AHR;

for c = 1:numchan
    big_data_AHR_ntr = zeros(numsubj*160*numfreq*numtime,8);
    r = 0;
    for VP = 1:numsubj
        for tr = 1:length(data{VP}(:,1,1,1))
            for f = 1:numfreq
                for t = 1:numtime
                    r = r + 1;
                    big_data_AHR_ntr(r,1) = (data{VP}(tr,c,f,t));
                    big_data_AHR_ntr(r,2) = VP+60;
                    big_data_AHR_ntr(r,3) = tr;
                    big_data_AHR_ntr(r,4) = c;
                    big_data_AHR_ntr(r,5) = f;
                    big_data_AHR_ntr(r,6) = t;
                    big_data_AHR_ntr(r,7) = 2; %ntr
                    big_data_AHR_ntr(r,8) = 3; 
                end
            end 
        end
    end
    
    % SAVE DATA
    big_data_AHR_ntr2=array2table(big_data_AHR_ntr, 'VariableNames',{'value','subj', 'trial',...
    'chan', 'freq', 'timepoint', 'emo', 'group'});
    big_data_AHR_ntr2 = big_data_AHR_ntr2(~(big_data_AHR_ntr2.group == 0 ),:); 

    writetable(big_data_AHR_ntr2, strcat('big_data_AHR_ntr_c',num2str(c),'.csv'))
    clear big_data_AHR_ntr
    clear big_data_AHR_ntr2
end
clearvars

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% now we have a lot of files that we can tidy up a bit so that the
% computation of the permutations runs a bit smoother


for c = 1:numchan% read in data and make it into a mat file so its smaller
    dataframe = [];

    files = dir(fullfile(path, strcat('/*_c', num2str(c), '.csv')));
        for k = 1 : length(files)
            dataframe = [dataframe; readtable(fullfile(files(k).folder, files(k).name))];
        end
        save(strcat('channel_',num2str(c),'.mat'), '-v7.3', 'dataframe');

end

% in theory, you can now delete all the "big_data" files