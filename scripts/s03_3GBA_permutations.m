%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% this script calculates pointwise linear mixed models and respective coefficients from n permutations
%%%%% a lot of the function logic was derived from the scripts used in Mendez-Bertolo, C., Moratti, S., Toledano, R., Lopez-Sosa, F., Mart�nez-Alvarez, R., Mah, Y. H., ... & Strange, B. A. (2016). A fast pathway for fear in human amygdala. Nature neuroscience, 19(8), 1041-1049.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the permutations are calculated in matlab because it is faster than R because of all the loops
% you need the functions glm_withinfreq_resec_parallel and permutation_test_glm for this part

clearvars


% CALCULATE THE PERMUTATIONS - THIS WILL TAKE A WHILE! 
% i am running them with a parallel pool to speed up computation time, but
% be careful of not maxing out your CPU to avoid overheating
% for me, 100 permutations need approx. 1 week on 8 cpu cores
% i would advise against calculating 1000 permutations at once on a "normal" desktop PC or laptop,
% rather split it or use a really powerful computer

%% permutation tests for subj x trial x chan x freq x time data
% the two stat functions extract the t values that are then used to evaluate
% statitical significance. if you want to evaluate a different parameter,
% you need to modify the functions

% Create a parallel pool with the desired number of workers (adjust this as needed)
numWorkers = 8; % Change to match the number of available/desired CPU cores
poolObj = gcp('nocreate');
if isempty(poolObj)
    parpool(numWorkers);
end

%% calculate lm on observed data
tic; % check how long the computation of one run takes
obslist = glm_withinfreq_resec_parallel(128,12,161); %chanxfreqxtime
elapsed_time = toc;
disp(['Elapsed time: ' num2str(elapsed_time) ' seconds']);

save('obslist.mat', '-v7.3', 'obslist');

%% calculate permutations and then do a lm again
clearvars
rndlist = permutation_test_glm(128,12,161,1000); %chanxfreqxtimexpermut
save('rndlist_1000.mat', '-v7.3', 'rndlist');
