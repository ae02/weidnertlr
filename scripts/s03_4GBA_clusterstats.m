%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% this script performs cluster based permutation tests on time frequency data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% Start by setting up Fieldtrip
addpath 'C:\Users\eweidner\sciebo\AttEmo_Material'
ft_startup

%%% first i craft the dataset that we need from those permutations, because
%%% i split the calculation of the permutations

% right now, all 5 permuted coefficients (main effects and interaction contrasts) are stored in the same struct, we need to make an
% array per stat and append all the perumtations and add channels as an
% extra dimension

rndlist1 = load('rndlist_500_1.mat');
rndlist2 = load('rndlist_500_2.mat');


% ideally these wouldn't be so many files but my PC needed a break sometimes
for c = 1:128
stat_ltlrvscon{c} = cat(3,rndlist1.rndlist{c}.ltlrvscon, rndlist2.rndlist{c}.ltlrvscon);
end

rndlist_ltlrvscon = cell2mat(cellfun(@(x)reshape(x,[1,1,size(x)]),stat_ltlrvscon,'un',0));
rndlist_ltlrvscon = squeeze(rndlist_ltlrvscon);
clear stat_ltlrvscon;


for c = 1:128
stat_rtlrvscon{c} = cat(3,rndlist1.rndlist{c}.rtlrvscon, rndlist2.rndlist{c}.rtlrvscon);
end
rndlist_rtlrvscon = cell2mat(cellfun(@(x)reshape(x,[1,1,size(x)]),stat_rtlrvscon,'un',0));
rndlist_rtlrvscon = squeeze(rndlist_rtlrvscon);
clear stat_rtlrvscon;

for c = 1:128
stat_emo{c} = cat(3,rndlist1.rndlist{c}.emo, rndlist2.rndlist{c}.emo);
end
rndlist_emo = cell2mat(cellfun(@(x)reshape(x,[1,1,size(x)]),stat_emo,'un',0));
rndlist_emo = squeeze(rndlist_emo);
clear stat_emo;


for c = 1:128
stat_int_ltlrvscon{c} = cat(3,rndlist1.rndlist{c}.int_ltlrvscon, rndlist2.rndlist{c}.int_ltlrvscon);
end
rndlist_int_ltlrvscon = cell2mat(cellfun(@(x)reshape(x,[1,1,size(x)]),stat_int_ltlrvscon,'un',0));
rndlist_int_ltlrvscon = squeeze(rndlist_int_ltlrvscon);
clear stat_int_ltlrvscon;


for c = 1:128
stat_int_rtlrvscon{c} = cat(3,rndlist1.rndlist{c}.int_rtlrvscon, rndlist2.rndlist{c}.int_rtlrvscon);
end
rndlist_int_rtlrvscon = cell2mat(cellfun(@(x)reshape(x,[1,1,size(x)]),stat_int_rtlrvscon,'un',0));
rndlist_int_rtlrvscon = squeeze(rndlist_int_rtlrvscon);
clear stat_int_rtlrvscon;


save rndlist_ltlrvscon rndlist_ltlrvscon;
save rndlist_rtlrvscon rndlist_rtlrvscon;
save rndlist_emo rndlist_emo;
save rndlist_int_ltlrvscon rndlist_int_ltlrvscon;
save rndlist_int_rtlrvscon rndlist_int_rtlrvscon;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% now do the clusterstats

% we have to load the rndlist and obslist

% first we have to load in the timepoints, freqs and channel labels so that
% we can localize the cluster later on. for that, i am just processing a
% random dataset and extracting the necessary data
% load('..\cleaned_CON.mat')
% %%%% freq analysis
% foi =[]; fw = []; toi = []; 
% 
% foi = 35:5:90; 
% tw  = 0.2*ones(length(foi),1)';
% fw  = 10*ones(length(foi),1)';
% toi = 'all'; 
%   
%   k=2.*tw.*fw;                   % this little formula determines the number of tapers that
% if floor(k)==k                 % that will be used
%     tap=k-1;
% else
%     tap=floor(k);
% end    
% 
% 
% %CON
% for VP = 1
%     cfg            = [];
%     cfg.channel    = 'all';
%     cfg.lpfilter = 'yes';
%     cfg.lpfilterord = 2;
%     cfg.lpfreq = 100;
%     cfg.hpfilter = 'yes';
%     cfg.hpfiltord = 1;
%     cfg.hpfreq = 0.1;
%     cfg.bsfilter = 'yes';
%     cfg.bsfreq = [49 50 51];
% 
%     cfg.channel = 'all'; % this is the default
%     cfg.reref = 'yes';
%     cfg.refmethod = 'avg';
%     cfg.refchannel = 'all';
%     ref_CON = ft_preprocessing(cfg, cleaned{VP});
%     
%     cfg = [];
%     cfg.resamplefs = 200;
%     ref_CON = ft_resampledata(cfg, ref_CON);
% 
%     cfg = [];
%     cfg.output    = 'pow';
%     cfg.channel   = 'all';
%     cfg.method    = 'mtmconvol';
%     cfg.taper     = 'dpss';
%     % cfg.keeptrials = 'yes'; %COMMENT THIS IF NOT NEEDED. 
%     cfg.foi       = foi; 
%     cfg.t_ftimwin = tw; %AP- length of time window in seconds
%     cfg.tapsmofrq = fw; %AP- the amount of spectral smoothing through multi-tapering
%     %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
%     cfg.toi       = toi; %AP
%     cfg.pad       = 'maxperlen'; %AP- length in seconds to which the data can be padded out
%     cfg.keeptrials = 'no'; % if you want to look at single subejct data
%    
%     data = ref_CON;
%     cfg.trials    = data.trialinfo==7|data.trialinfo==107;
%     HFfreq_neg = ft_freqanalysis(cfg, data);
%     HFfreq_neg.freq = foi; 
% 
%     %%% tidying stuff
%     cfg = [];
%     cfg.baseline = [-0.5 0];
%     cfg.baselinetype = 'relchange';
%     freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
% 
%     cfg=[];
%     cfg.avgoverchan = 'no';
%     cfg.channel = {'A*', 'B*', 'C*', 'D*'};
%     freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
% 
%     cfg=[];
%     cfg.appenddim = 'rpt';
%     cfg.parameter = 'powspctrm';
%     freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
% 
%     cfg=[];
%     cfg.avgoverrpt = 'yes';
%     cfg.latency = [0 0.8];
%     freq_neg_CON = ft_selectdata(cfg,freq_ses11);
%     
%     clear HFfreq*
% end
% 
% clear freq_ses*;
% clear freq_tmp*;
% clear freq_HF*;
% 
% % save relevant variables to avoid doing this again
% label = freq_neg_CON.label;
% time = freq_neg_CON.time;
% freq = freq_neg_CON.freq;

% save label label;
% save time time;
% save freq freq;

%% LOAD STATS
load('label.mat');
load('time.mat');
load('freq.mat');
load('obslist.mat');


% when loading in the permutation data we have to do a little bit of
% reshaping and subsetting to make it work for the clusterstat function

% main effect emotion
load('rndlist_emo.mat');
statobs_emo = obslist{1};

statrnd = reshape(rndlist_emo, [numel(statobs_emo), size(rndlist_emo,4)]);

statobs2 = statobs_emo(:);
emo.statobs = statobs2; 
emo.statrnd = statrnd; 

% main effect ltlr x con 
load('rndlist_ltlrvscon.mat');
statobs_ltlrvscon = obslist{2};

statrnd = reshape(rndlist_ltlrvscon, [numel(statobs_ltlrvscon), size(rndlist_ltlrvscon,4)]);

statobs2 = statobs_ltlrvscon(:);
ltlr.statobs = statobs2; 
ltlr.statrnd = statrnd; 

% main effect rtlr x con 
load('rndlist_rtlrvscon.mat');
statobs_rtlrvscon = obslist{3};

statrnd = reshape(rndlist_rtlrvscon, [numel(statobs_rtlrvscon), size(rndlist_rtlrvscon,4)]);

statobs2 = statobs_rtlrvscon(:);
rtlr.statobs = statobs2; 
rtlr.statrnd = statrnd; 


% interaction ltlr x con - emo
load('rndlist_int_ltlrvscon.mat');
statobs_int_ltlrvscon = obslist{4};

statrnd = reshape(rndlist_int_ltlrvscon, [numel(statobs_int_ltlrvscon), size(rndlist_int_ltlrvscon,4)]);

statobs2 = statobs_int_ltlrvscon(:);
int_ltlr.statobs = statobs2; 
int_ltlr.statrnd = statrnd; 

% interaction rtlr x con - emo
load('rndlist_int_rtlrvscon.mat');
statobs_int_rtlrvscon = obslist{5};

statrnd = reshape(rndlist_int_rtlrvscon, [numel(statobs_int_rtlrvscon), size(rndlist_int_rtlrvscon,4)]);

statobs2 = statobs_int_rtlrvscon(:);
int_rtlr.statobs = statobs2; 
int_rtlr.statrnd = statrnd; 

%%% for the clusterstats, we need to know the spatial dsitrubution of the channels
load('neighbours.mat');

% cfg = [];
% cfg.neighbours = neighbours;
% cfg.channel = label; % order is important here, must be the same asthe data
% connectivity = channelconnectivity(cfg);
% 
% save connectivity connectivity;

load('connectivity.mat');

%% CLUSTER STATS

% for the glm we have to calculate the critical t value. this right here is not the
% most elegant way of doing it. i am basically running the lm once with
% help of the permutation script and extract the df from all the
% coefficients that are given by matlab. you can use the permutation
% function to achieve this

% for me, the df are 14490

thresh_redux = 0.01; %decide what cluster threshold you want to use

% Compute your degrees of freedom and thresholds
df1main = 14490; 
t = [1:0.1:200]; 
Pmain = 1-tcdf(t,df1main); 
index = find(Pmain <= thresh_redux ); 
tcritmain = t(index(1)); 

% set different cfg options
% emo main
cfg=[]; 
cfg.clustercritval = tcritmain; 
cfg.tail = 0; 
cfg.clustertail = 0; 
cfg.clusterthreshold = 'parametric'; 
cfg.dim = size(statobs_emo);
cfg.neighbours = []; 
cfg.avgoverchan = 'no'; 
cfg.avgoverfreq = 'no'; 
cfg.avgovertime = 'no'; 
cfg.feedback = 'yes'; 
cfg.channel = 'all';
cfg.numrandomization = 1000; 
cfg.clusterstatistic = 'maxsum';
cfg.connectivity = connectivity;
cfg.minnbchan = 2;

% do the computations (this if for emotion)
stat_emo = clusterstat(cfg, emo.statrnd, emo.statobs);
stat_emo.posclusterslabelmat = reshape(stat_emo.posclusterslabelmat,size(statobs_emo));
stat_emo.negclusterslabelmat = reshape(stat_emo.negclusterslabelmat,size(statobs_emo));
stat_emo.prob = reshape(stat_emo.prob,size(statobs_emo));

% the following part tidies the data and puts all the relevant infos into a
% new table that you can extract statistical parameters and cluster
% location from
for e = 1:length(stat_emo.posclusters)
    if stat_emo.posclusters(e).prob <= 0.1

        freqstats.mask = (stat_emo.posclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_emo = label(squeeze(sum(sum3,2))>0);
    
    effect_emo_pos(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_emo},stat_emo.posclusters(e).clusterstat, stat_emo.posclusters(e).prob];
    end
end

for e = 1:length(stat_emo.negclusters)
    if stat_emo.negclusters(e).prob <= 0.1

        freqstats.mask = (stat_emo.negclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_emo = label(squeeze(sum(sum3,2))>0);
    
    effect_emo_neg(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_emo},stat_emo.negclusters(e).clusterstat, stat_emo.negclusters(e).prob];
    end
end


% main effect ltlr
cfg=[]; 
cfg.clustercritval = tcritmain; 
cfg.tail = 0; 
cfg.clustertail = 0; 
cfg.clusterthreshold = 'parametric'; 
cfg.dim = size(statobs_ltlrvscon);
cfg.neighbours = []; 
cfg.avgoverchan = 'yes'; 
cfg.avgoverfreq = 'no'; 
cfg.avgovertime = 'no'; 
cfg.feedback = 'yes'; 
cfg.channel = 'all';
cfg.numrandomization = 1000; 
cfg.clusterstatistic = 'maxsum';
cfg.connectivity = connectivity;
cfg.minnbchan = 2;

stat_ltlr = clusterstat(cfg, ltlr.statrnd, ltlr.statobs);
stat_ltlr.posclusterslabelmat = reshape(stat_ltlr.posclusterslabelmat,size(statobs_ltlrvscon));
stat_ltlr.negclusterslabelmat = reshape(stat_ltlr.negclusterslabelmat,size(statobs_ltlrvscon));
stat_ltlr.prob = reshape(stat_ltlr.prob,size(statobs_ltlrvscon));

for e = 1:length(stat_ltlr.posclusters)
    if stat_ltlr.posclusters(e).prob <= 0.1

        freqstats.mask = (stat_ltlr.posclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_ltlr_pos = label(squeeze(sum(sum3,2))>0);
    
    effect_ltlr_pos(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_ltlr_pos},stat_ltlr.posclusters(e).clusterstat, stat_ltlr.posclusters(e).prob];
    end
end

for e = 1:length(stat_ltlr.negclusters)
    if stat_ltlr.negclusters(e).prob <= 0.1

        freqstats.mask = (stat_ltlr.negclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_ltlr_neg = label(squeeze(sum(sum3,2))>0);
    
    effect_ltlr_neg(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_ltlr_neg},stat_ltlr.negclusters(e).clusterstat, stat_ltlr.negclusters(e).prob];
    end
end


% main effect rtlr
cfg=[]; 
cfg.clustercritval = tcritmain; 
cfg.tail = 0; 
cfg.clustertail = 0; 
cfg.clusterthreshold = 'parametric'; 
cfg.dim = size(statobs_rtlrvscon);
cfg.neighbours = []; 
cfg.avgoverchan = 'yes'; 
cfg.avgoverfreq = 'no'; 
cfg.avgovertime = 'no'; 
cfg.feedback = 'yes'; 
cfg.channel = 'all';
cfg.numrandomization = 1000; 
cfg.clusterstatistic = 'maxsum';
cfg.connectivity = connectivity;
cfg.minnbchan = 2;

stat_rtlr = clusterstat(cfg, rtlr.statrnd, rtlr.statobs);
stat_rtlr.posclusterslabelmat = reshape(stat_rtlr.posclusterslabelmat,size(statobs_rtlrvscon));
stat_rtlr.negclusterslabelmat = reshape(stat_rtlr.negclusterslabelmat,size(statobs_rtlrvscon));
stat_rtlr.prob = reshape(stat_rtlr.prob,size(statobs_rtlrvscon));

for e = 1:length(stat_rtlr.posclusters)
    if stat_rtlr.posclusters(e).prob <= 0.1

        freqstats.mask = (stat_rtlr.posclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_rtlr_pos = label(squeeze(sum(sum3,2))>0);
    
    effect_rtlr_pos(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_rtlr_pos},stat_rtlr.posclusters(e).clusterstat, stat_rtlr.posclusters(e).prob];
    end
end

for e = 1:length(stat_rtlr.negclusters)
    if stat_rtlr.negclusters(e).prob <= 0.1

        freqstats.mask = (stat_rtlr.negclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_rtlr_neg = label(squeeze(sum(sum3,2))>0);
    
    effect_rtlr_neg(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_rtlr_neg},stat_rtlr.negclusters(e).clusterstat, stat_rtlr.negclusters(e).prob];
    end
end


% emo x group interaction ltlr
cfg=[]; 
cfg.clustercritval = tcritmain; 
cfg.tail = 0; 
cfg.clustertail = 0; 
cfg.clusterthreshold = 'parametric'; 
cfg.dim = size(statobs_int_ltlrvscon);
cfg.neighbours = []; 
cfg.avgoverchan = 'yes'; 
cfg.avgoverfreq = 'no'; 
cfg.avgovertime = 'no'; 
cfg.feedback = 'yes'; 
cfg.channel = 'all';
cfg.numrandomization = 1000; 
cfg.clusterstatistic = 'maxsum';
cfg.connectivity = connectivity;
cfg.minnbchan = 2;

stat_int_ltlr = clusterstat(cfg, int_ltlr.statrnd, int_ltlr.statobs);
stat_int_ltlr.posclusterslabelmat = reshape(stat_int_ltlr.posclusterslabelmat,size(statobs_int_ltlrvscon));
stat_int_ltlr.negclusterslabelmat = reshape(stat_int_ltlr.negclusterslabelmat,size(statobs_int_ltlrvscon));
stat_int_ltlr.prob = reshape(stat_int_ltlr.prob,size(statobs_int_ltlrvscon));

for e = 1:length(stat_int_ltlr.posclusters)
    if stat_int_ltlr.posclusters(e).prob <= 0.1

        freqstats.mask = (stat_int_ltlr.posclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_int_ltlr_pos = label(squeeze(sum(sum3,2))>0);
    
    effect_int_ltlr_pos(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_int_ltlr_pos},stat_int_ltlr.posclusters(e).clusterstat, stat_int_ltlr.posclusters(e).prob];
    end
end

for e = 1:length(stat_int_ltlr.negclusters)
    if stat_int_ltlr.negclusters(e).prob <= 0.1

        freqstats.mask = (stat_int_ltlr.negclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_int_ltlr_neg = label(squeeze(sum(sum3,2))>0);
    
    effect_int_ltlr_neg(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_int_ltlr_neg},stat_int_ltlr.negclusters(e).clusterstat, stat_int_ltlr.negclusters(e).prob];
    end
end


% emo x group interaction  rtlr
cfg=[]; 
cfg.clustercritval = tcritmain; 
cfg.tail = 0; 
cfg.clustertail = 0; 
cfg.clusterthreshold = 'parametric'; 
cfg.dim = size(statobs_int_rtlrvscon);
cfg.neighbours = []; 
cfg.avgoverchan = 'yes'; 
cfg.avgoverfreq = 'no'; 
cfg.avgovertime = 'no'; 
cfg.feedback = 'yes'; 
cfg.channel = 'all';
cfg.numrandomization = 1000; 
cfg.clusterstatistic = 'maxsum';
cfg.connectivity = connectivity;
cfg.minnbchan = 2;

stat_int_rtlr = clusterstat(cfg, int_rtlr.statrnd, int_rtlr.statobs);
stat_int_rtlr.posclusterslabelmat = reshape(stat_int_rtlr.posclusterslabelmat,size(statobs_int_rtlrvscon));
stat_int_rtlr.negclusterslabelmat = reshape(stat_int_rtlr.negclusterslabelmat,size(statobs_int_rtlrvscon));
stat_int_rtlr.prob = reshape(stat_int_rtlr.prob,size(statobs_int_rtlrvscon));

for e = 1:length(stat_int_rtlr.posclusters)
    if stat_int_rtlr.posclusters(e).prob <= 0.1

        freqstats.mask = (stat_int_rtlr.posclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_int_rtlr_pos = label(squeeze(sum(sum3,2))>0);
    
    effect_int_rtlr_pos(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_int_rtlr_pos},stat_int_rtlr.posclusters(e).clusterstat, stat_int_rtlr.posclusters(e).prob];
    end
end

for e = 1:length(stat_int_rtlr.negclusters)
    if stat_int_rtlr.negclusters(e).prob <= 0.1

        freqstats.mask = (stat_int_rtlr.negclusterslabelmat ==e);%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

        sum1 = squeeze(sum(freqstats.mask,2));
        clust_time = time(squeeze(sum(sum1,1))>0);

        sum2 = squeeze(sum(freqstats.mask,3));
        clust_freq = freq(squeeze(sum(sum2,1))>0);

        sum3 = squeeze(sum(freqstats.mask,2));
        clust_chan_int_rtlr_neg = label(squeeze(sum(sum3,2))>0);
    
    effect_int_rtlr_neg(e,:) = [min(clust_time), max(clust_time), min(clust_freq), max(clust_freq),{clust_chan_int_rtlr_neg},stat_int_rtlr.negclusters(e).clusterstat, stat_int_rtlr.negclusters(e).prob];
    end
end




%% now we can plot the data

% caclulate averages that we will later use to plot the time frequency
% cluster data

% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR

for VP = 1:length(freq_neg_AHL)
    cfg = [];
    freq_neg_AHR{VP} = ft_freqdescriptives(cfg, freq_neg_AHR{VP});
    freq_ntr_AHR{VP} = ft_freqdescriptives(cfg, freq_ntr_AHR{VP});
    freq_neg_CON{VP} = ft_freqdescriptives(cfg, freq_neg_CON{VP});
    freq_ntr_CON{VP} = ft_freqdescriptives(cfg, freq_ntr_CON{VP});
    freq_neg_AHL{VP} = ft_freqdescriptives(cfg, freq_neg_AHL{VP});
    freq_ntr_AHL{VP} = ft_freqdescriptives(cfg, freq_ntr_AHL{VP});
end
    
cfg=[];
cfg.keepindividual='yes';
f_neg_AHR = ft_freqgrandaverage(cfg,freq_neg_AHR{[1:3 5:8 10:20]});
f_ntr_AHR = ft_freqgrandaverage(cfg,freq_ntr_AHR{[1:3 5:8 10:20]});
f_neg_CON = ft_freqgrandaverage(cfg,freq_neg_CON{[1:17 19]});
f_ntr_CON = ft_freqgrandaverage(cfg,freq_ntr_CON{[1:17 19]});
f_neg_AHL = ft_freqgrandaverage(cfg,freq_neg_AHL{[1:3 5:14 16:20]});
f_ntr_AHL = ft_freqgrandaverage(cfg,freq_ntr_AHL{[1:3 5:14 16:20]});

cfg = [];
cfg.operation = 'x1-x2';
cfg.parameter = 'powspctrm';
gavg_diff_AHR = ft_math(cfg, f_neg_AHR, f_ntr_AHR);
gavg_diff_CON = ft_math(cfg, f_neg_CON, f_ntr_CON);
gavg_diff_AHL = ft_math(cfg, f_neg_AHL, f_ntr_AHL);

% select GBA in TOI
cfg=[];
cfg.latency = [0 0.8];
cfg.frequency = [35 90];
f_neg_AHR = ft_selectdata(cfg,f_neg_AHR);
f_ntr_AHR = ft_selectdata(cfg,f_ntr_AHR);
f_neg_CON = ft_selectdata(cfg,f_neg_CON);
f_ntr_CON = ft_selectdata(cfg,f_ntr_CON);
f_neg_AHL = ft_selectdata(cfg,f_neg_AHL);
f_ntr_AHL = ft_selectdata(cfg,f_ntr_AHL);
% 
gavg_diff_CON = ft_selectdata(cfg,gavg_diff_CON);
gavg_diff_AHR = ft_selectdata(cfg,gavg_diff_AHR);
gavg_diff_AHL = ft_selectdata(cfg,gavg_diff_AHL);

%% PLOT 

% i am using the colorbrewer colormaps: https://de.mathworks.com/matlabcentral/fileexchange/45208-colorbrewer-attractive-and-distinctive-colormaps

%%% Plot the interaction

% mask effct

freqstats.mask = stat_int_rtlr.negclusterslabelmat ==1;%|stat.posclusterslabelmat ==2|stat.posclusterslabelmat ==3|stat.posclusterslabelmat ==4|stat.posclusterslabelmat ==5);%|stat.posclusterslabelmat ==6|stat.posclusterslabelmat ==7); %plotting cluster one (i.e row1)

sum1 = squeeze(sum(freqstats.mask,2));
clust_time = time(squeeze(sum(sum1,1))>0);

sum2 = squeeze(sum(freqstats.mask,3));
clust_freq = freq(squeeze(sum(sum2,1))>0);

sum3 = squeeze(sum(freqstats.mask,2));
clust_chan_int_rtlr_neg = label(squeeze(sum(sum3,2))>0);
    

% randomnly write all the necessary info into a variable, you can also make a new one
gavg_diff_AHR.prob = stat_int_rtlr.prob;
gavg_diff_AHR.stat = statobs_int_rtlrvscon;

gavg_diff_AHR.posclusters = stat_int_rtlr.posclusters;
gavg_diff_AHR.posclusterslabelmat = stat_int_rtlr.posclusterslabelmat;
gavg_diff_AHR.mask = freqstats.mask;

% clusterplot t-values topography
cfg=[];
cfg.latency = [min(clust_time) max(clust_time)];
cfg.frequency = [min(clust_freq) max(clust_freq)];
cfg.avgovertime = 'yes';
cfg.avgoverfreq = 'yes';
ygavg_diff_AHR = ft_selectdata(cfg,gavg_diff_AHR); % this is arbitrary because i am using this to write everything i need for plotting into, you could also make a dedictaed variable

addpath ..\colorschemes; % this is what we need for the brewermap colormaps
gavg_diff_AHR.mask(gavg_diff_AHR.mask > 0) = 1;

figure('Color', [1 1 1]);
cfg = [];
cfg.parameter = 'stat';
cfg.layout    = 'biosemi128.lay';
cfg.colormap = flipud(colormap(brewermap([],'RdBu')));
cfg.colorbar = 'yes';
cfg.layout = 'biosemi128.lay';
cfg.colorbartext = 't-value';
cfg.comment = 'no';
cfg.zlim = [-2.5 2.5];

ft_topoplotTFR(cfg, ygavg_diff_AHR);
set(gca,'fontsize',20);
saveas (gcf, ['topo_cluster_int.jpg']);


%topographies of raw averages within the time frequency cluster
figure('Color', [1 1 1]);
cfg.zlim      = [-0.03 0.03];
cfg.parameter = 'powspctrm';
cfg.colormap = flipud(colormap(brewermap([],'Spectral')));
cfg.xlim = [min(clust_time) max(clust_time)];
cfg.ylim = [min(clust_freq) max(clust_freq)];

figure('Color', [1 1 1]);ft_topoplotTFR(cfg, f_neg_CON);set(gca,'fontsize',20);
saveas (gcf, ['topo_neg_con.jpg']);
figure('Color', [1 1 1]);ft_topoplotTFR(cfg, f_ntr_CON);set(gca,'fontsize',20);
saveas (gcf, ['topo_ntr_con.jpg']);
figure('Color', [1 1 1]);ft_topoplotTFR(cfg, f_neg_AHR);set(gca,'fontsize',20);
saveas (gcf, ['topo_neg_ahr.jpg']);
figure('Color', [1 1 1]);ft_topoplotTFR(cfg, f_ntr_AHR);set(gca,'fontsize',20);
saveas (gcf, ['topo_ntr_ahr.jpg']);
figure('Color', [1 1 1]);ft_topoplotTFR(cfg, f_neg_AHL);set(gca,'fontsize',20);
saveas (gcf, ['topo_neg_ahl.jpg']);
figure('Color', [1 1 1]);ft_topoplotTFR(cfg, f_ntr_AHL);set(gca,'fontsize',20);
saveas (gcf, ['topo_ntr_ahl.jpg']);

% plot time-frequency distribution of t-values
cfg=[];
gavg_diff_AHR.mask = freqstats.mask;
cfg.channel = clust_chan_int_rtlr_neg;
cfg.avgoverchan = 'yes';
zgavg_diff_AHR = ft_selectdata(cfg,gavg_diff_AHR);
zgavg_diff_AHR.mask(zgavg_diff_AHR.mask > 0) = 1;
zgavg_diff_AHR.mask = logical(zgavg_diff_AHR.mask);

figure('Position',[181 389 901 472], 'Color', [1 1 1]);
cfg = [];
cfg.parameter = 'stat';
cfg.colorbar = 'yes';
cfg.zlim = [-2.5 2.5];
cfg.maskparameter = 'mask';
cfg.maskstyle = 'outline';
cfg.colormap = flipud(colormap(brewermap([],'RdBu')));
cfg.title = ' ';
cfg.layout = 'biosemi128.lay';
xlabel('Time (s)','FontSize',20);ylabel('Frequency (Hz)','FontSize',20);
set(gca,'fontsize',20)
ft_singleplotTFR(cfg, zgavg_diff_AHR);
saveas (gcf, ['singleplot_cluster_int.jpg']);



%% export the cluster data for plotting barplots in R

cfg=[];
cfg.latency = [min(clust_time) max(clust_time)];
cfg.avgoverfreq='yes';
cfg.frequency = [min(clust_freq) max(clust_freq)];
cfg.channel = clust_chan_int_rtlr_neg;
cfg.avgoverchan = 'yes';
cfg.avgovertime='yes';
cfg.nanmean = 'yes';
freq1 = ft_selectdata(cfg,f_neg_CON);
freq2 = ft_selectdata(cfg,f_ntr_CON);
freq3 = ft_selectdata(cfg,f_neg_AHR);
freq4 = ft_selectdata(cfg,f_ntr_AHR);
freq5 = ft_selectdata(cfg,f_neg_AHL);
freq6 = ft_selectdata(cfg,f_ntr_AHL);

y = [freq1.powspctrm freq2.powspctrm, freq3.powspctrm freq4.powspctrm,freq5.powspctrm freq6.powspctrm];
table_y = array2table(num2cell(y), 'VariableNames', {'neg_CON','ntr_CON','neg_AHR','ntr_AHR','neg_AHL','ntr_AHL'});
writetable(table_y, 'table_y.csv')


%% post hoc tests
% for post hoc tests, we will start from scratch, load in data and extract data from clusters, averaged across timepoints, channels and frequencies, but preserving single trials

% first do a freq analysis and select time-window, channels, and freqs of
% interest for further export

% i  export all three groups so that the model is as close to the original
% as possible

clearvars 

% this is very unelegant but it works (for now): select tf-cluster based on stats
cluster_chan = {'A26','A27','A28','A29','B6','B7','B8','B9','B10','B11','B12','B13'};
cluster_ime = [0.095 0.3]; %this is the time window of the interaction
cluster_freq = [60 80];

%rtlr (AHR)
load('..\cleaned_AHR.mat');

%%pre-allocate memory to save time
freq_neg_AHR = cell(1,length(cleaned));
freq_neg_AHR{1,length(cleaned)} = [];

freq_ntr_AHR = cell(1,length(cleaned));
freq_ntr_AHR{1,length(cleaned)} = [];

foi = 35:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
tap=k-1;
else
tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; % length of time window in seconds
    cfg.tapsmofrq = fw; %- the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; 
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'yes'; % if you want to look at single trial data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);

    cfg=[];
    cfg.avgoverrpt = 'no';
    fr_neg_AHR = ft_selectdata(cfg,freq_ses11); %usually, this is already an array
    fr_ntr_AHR = ft_selectdata(cfg,freq_ses12);
    
    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
    
    % this next bit i added for exporting the data for R, omit this if you just
    % want to visualize the data

    cfg = [];
    cfg.latency = cluster_time; %this is the time window of the interaction
    cfg.channel = cluster_chan;
    cfg.frequency = cluster_freq;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    cfg.avgoverfreq = 'yes';
    freq_neg_AHR{VP} = ft_selectdata(cfg, fr_neg_AHR);
    freq_ntr_AHR{VP} = ft_selectdata(cfg, fr_ntr_AHR);
end

% HC
load('..\cleaned_CON.mat');

%%pre-allocate memory to save time
freq_neg_CON = cell(1,length(cleaned));
freq_neg_CON{1,length(cleaned)} = [];

freq_ntr_CON = cell(1,length(cleaned));
freq_ntr_CON{1,length(cleaned)} = [];

foi = 35:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
tap=k-1;
else
tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; %length of time window in seconds
    cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; %
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'yes'; % if you want to look at single trial data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);

    cfg=[];
    cfg.avgoverrpt = 'no';
    fr_neg_CON = ft_selectdata(cfg,freq_ses11); %usually, this is already an array
    fr_ntr_CON = ft_selectdata(cfg,freq_ses12);
    
    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
    
    % this next bit i added for exporting the data for R, omit this if you just
    % want to visualize the data

    cfg = [];
    cfg.latency = clust_time; %this is the time window of the interaction
    cfg.channel = clust_chan;
    cfg.frequency = clust_freq;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    cfg.avgoverfreq = 'yes';
    freq_neg_CON{VP} = ft_selectdata(cfg, fr_neg_CON);
    freq_ntr_CON{VP} = ft_selectdata(cfg, fr_ntr_CON);
end

% lTLR
load('..\cleaned_AHL.mat');

%%pre-allocate memory to save time
freq_neg_AHL = cell(1,length(cleaned));
freq_neg_AHL{1,length(cleaned)} = [];

freq_ntr_AHL = cell(1,length(cleaned));
freq_ntr_AHL{1,length(cleaned)} = [];

foi = 35:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
tap=k-1;
else
tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; %length of time window in seconds
    cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; 
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'yes'; % if you want to look at single trial data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);

    cfg=[];
    cfg.avgoverrpt = 'no';
    fr_neg_AHL = ft_selectdata(cfg,freq_ses11); %usually, this is already an array
    fr_ntr_AHL = ft_selectdata(cfg,freq_ses12);
    
    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
    
    % this next bit i added for exporting the data for R, omit this if you just
    % want to visualize the data

    cfg = [];
    cfg.latency = clust_time; %this is the time window of the interaction
    cfg.channel = clust_chan;
    cfg.frequency = clust_freq;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    cfg.avgoverfreq = 'yes';
    freq_neg_AHL{VP} = ft_selectdata(cfg, fr_neg_AHL);
    freq_ntr_AHL{VP} = ft_selectdata(cfg, fr_ntr_AHL);
end



%% make the data ready for export
int_con = [];
for VP = [1:17 19] % con
    int_neg = [];
    int_ntr = [];

    data_neg =  freq_neg_CON{VP}.powspctrm;
    int_neg(:,3) = data_neg;
    int_neg(1:length(data_neg),1) = VP; % subjID
    int_neg(1:length(data_neg),2) = 1; % emo
    int_neg(1:length(data_neg),4) = 1; % group
    int_neg(1:length(data_neg),5) = 1:length(data_neg); % trial

    data_ntr =  freq_ntr_CON{VP}.powspctrm;
    int_ntr(:,3) = data_ntr;
    int_ntr(1:length(data_ntr),1) = VP; % subjID
    int_ntr(1:length(data_ntr),2) = 2; % emo
    int_ntr(1:length(data_ntr),4) = 1; % group (1 = con, 2 = ahl, 3 = ahr)
    int_ntr(1:length(data_ntr),5) = 1:length(data_ntr); % trial

    int_con = [int_con; int_neg; int_ntr];
end

int_ahr = [];
for VP = [1:3 5:8 10:20] % ahr
    int_neg = [];
    int_ntr = [];

    data_neg =  freq_neg_AHR{VP}.powspctrm;
    int_neg(:,3) = data_neg;
    int_neg(1:length(data_neg),1) = VP+60; % subjID
    int_neg(1:length(data_neg),2) = 1; % emo
    int_neg(1:length(data_neg),4) = 3; % group
    int_neg(1:length(data_neg),5) = 1:length(data_neg); % trial

    data_ntr =  freq_ntr_AHR{VP}.powspctrm;
    int_ntr(:,3) = data_ntr;
    int_ntr(1:length(data_ntr),1) = VP+60; % subjID
    int_ntr(1:length(data_ntr),2) = 2; % emo
    int_ntr(1:length(data_ntr),4) = 3; % group (1 = con, 2 = ahl, 3 = ahr)
    int_ntr(1:length(data_ntr),5) = 1:length(data_ntr); % trial

    int_ahr = [int_ahr; int_neg; int_ntr];
end

int_ahl = [];
for VP = [1:3 5:14 16:20] % ahl
    int_neg = [];
    int_ntr = [];

    data_neg =  freq_neg_AHL{VP}.powspctrm;
    int_neg(:,3) = data_neg;
    int_neg(1:length(data_neg),1) = VP+36; % subjID
    int_neg(1:length(data_neg),2) = 1; % emo
    int_neg(1:length(data_neg),4) = 2; % group
    int_neg(1:length(data_neg),5) = 1:length(data_neg); % trial

    data_ntr =  freq_ntr_AHL{VP}.powspctrm;
    int_ntr(:,3) = data_ntr;
    int_ntr(1:length(data_ntr),1) = VP+36; % subjID
    int_ntr(1:length(data_ntr),2) = 2; % emo
    int_ntr(1:length(data_ntr),4) = 2; % group (1 = con, 2 = ahl, 3 = ahr)
    int_ntr(1:length(data_ntr),5) = 1:length(data_ntr); % trial

    int_ahl = [int_ahl;int_neg; int_ntr];
end

interaction_export = [int_con; int_ahl; int_ahr];
interaction_export_table = array2table(num2cell([int_con; int_ahl; int_ahr]), 'VariableNames', {'subject','emo','value','group', 'trial'});
writetable(interaction_export_table, 'interaction_export_table.csv')

