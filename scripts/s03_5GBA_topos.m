%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% this script plots topographies of GBA across time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% first set up fieldtrip toolbox, for more information visit: https://www.fieldtriptoolbox.org/
%addpath PATHTOSTARTUPFUNCTION
%ft_startup
% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR

% the data has to be processed again, this time, we don't keep all the
% trials
addpath C:\Users\enyaw\sciebo\AttEmo_Material\
ft_startup


%% CON
load('..\cleaned_CON.mat');

%%pre-allocate memory to save time
freq_neg_CON = cell(1,length(cleaned));
freq_neg_CON{1,length(cleaned)} = [];

freq_ntr_CON = cell(1,length(cleaned));
freq_ntr_CON{1,length(cleaned)} = [];

foi = 30:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
    tap=k-1;
else
    tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; % length of time window in seconds
    cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; 
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'no'; % if you want to look at single subejct data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);
% 
    cfg=[];
    cfg.avgoverrpt = 'no';
    freq_neg_CON{VP} = ft_selectdata(cfg,freq_ses11);
    freq_ntr_CON{VP} = ft_selectdata(cfg,freq_ses12);

    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AHR
load('..\cleaned_AHR.mat');


%%pre-allocate memory to save time
freq_neg_AHR = cell(1,length(cleaned));
freq_neg_AHR{1,length(cleaned)} = [];

freq_ntr_AHR = cell(1,length(cleaned));
freq_ntr_AHR{1,length(cleaned)} = [];

foi = 30:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
tap=k-1;
else
tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; % length of time window in seconds
    cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; 
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'no'; % if you want to look at single subejct data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);
% 
    cfg=[];
    cfg.avgoverrpt = 'no';
    freq_neg_AHR{VP} = ft_selectdata(cfg,freq_ses11);
    freq_ntr_AHR{VP} = ft_selectdata(cfg,freq_ses12);

    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AHL
load('..\cleaned_AHL.mat');


%%pre-allocate memory to save time
freq_neg_AHL = cell(1,length(cleaned));
freq_neg_AHL{1,length(cleaned)} = [];

freq_ntr_AHL = cell(1,length(cleaned));
freq_ntr_AHL{1,length(cleaned)} = [];

foi = 30:5:90; 
tw  = 0.2*ones(length(foi),1)';
fw  = 10*ones(length(foi),1)';
toi = 'all'; 

k=2.*tw.*fw;                   % this little formula determines the number of tapers that
if floor(k)==k                 % that will be used
tap=k-1;
else
tap=floor(k);
end    

for VP = 1:length(cleaned)
    cfg = [];
    cfg.output    = 'pow';
    cfg.channel   = 'all';
    cfg.method    = 'mtmconvol';
    cfg.taper     = 'dpss';
    cfg.foi       = foi; 
    cfg.t_ftimwin = tw; % length of time window in seconds
    cfg.tapsmofrq = fw; % the amount of spectral smoothing through multi-tapering
    %4Hz smoothing is +- 4Hz i.e a 8 Hz smoothing box
    cfg.toi       = toi; 
    cfg.pad       = 'maxperlen'; % length in seconds to which the data can be padded out
    cfg.keeptrials = 'no'; % if you want to look at single subejct data
   
    data = cleaned{VP};
    cfg.trials    = data.trialinfo==7|data.trialinfo==107;
    HFfreq_neg = ft_freqanalysis(cfg, data);
    HFfreq_neg.freq = foi; 
    cfg.trials    = data.trialinfo==6|data.trialinfo==106;
    HFfreq_ntr = ft_freqanalysis(cfg, data);
    HFfreq_ntr.freq = foi;

    %%% tidying stuff
    cfg = [];
    cfg.baseline = [-0.6 -0.1];
    cfg.baselinetype = 'relchange';
    freq_HFneg = ft_freqbaseline(cfg,HFfreq_neg);
    freq_HFntr = ft_freqbaseline(cfg,HFfreq_ntr);

    cfg=[];
    cfg.avgoverchan = 'no';
    cfg.channel = {'A*', 'B*', 'C*', 'D*'};
    freq_tmp11 = ft_selectdata(cfg,freq_HFneg);
    freq_tmp12 = ft_selectdata(cfg,freq_HFntr);

    cfg=[];
    cfg.appenddim = 'rpt';
    cfg.parameter = 'powspctrm';
    freq_ses11 = ft_appendfreq(cfg,freq_tmp11);
    freq_ses12 = ft_appendfreq(cfg,freq_tmp12);
% 
    cfg=[];
    cfg.avgoverrpt = 'no';
    freq_neg_AHL{VP} = ft_selectdata(cfg,freq_ses11);
    freq_ntr_AHL{VP} = ft_selectdata(cfg,freq_ses12);

    clear HFfreq*
    clear freq_ses*;
    clear freq_tmp*;
    clear freq_HF*;
end
%% caclulate averages that we will later use to plot the time frequency cluster data

for VP = 1:length(freq_neg_AHL)
    cfg=[];
    freq_neg_AHR{VP} = ft_freqdescriptives(cfg,freq_neg_AHR{VP});
    freq_ntr_AHR{VP} = ft_freqdescriptives(cfg,freq_ntr_AHR{VP});
    freq_neg_CON{VP}= ft_freqdescriptives(cfg,freq_neg_CON{VP});
    freq_ntr_CON{VP} = ft_freqdescriptives(cfg,freq_ntr_CON{VP});
    freq_neg_AHL{VP} = ft_freqdescriptives(cfg,freq_neg_AHL{VP});
    freq_ntr_AHL{VP} = ft_freqdescriptives(cfg,freq_ntr_AHL{VP});
end

cfg=[];
cfg.keepindividual='yes';
f_neg_AHR = ft_freqgrandaverage(cfg,freq_neg_AHR{[1:3 5:8 10:20]});
f_ntr_AHR = ft_freqgrandaverage(cfg,freq_ntr_AHR{[1:3 5:8 10:20]});
f_neg_CON = ft_freqgrandaverage(cfg,freq_neg_CON{[1:17 19]});
f_ntr_CON = ft_freqgrandaverage(cfg,freq_ntr_CON{[1:17 19]});
f_neg_AHL = ft_freqgrandaverage(cfg,freq_neg_AHL{[1:3 5:14 16:20]});
f_ntr_AHL = ft_freqgrandaverage(cfg,freq_ntr_AHL{[1:3 5:14 16:20]});

clear freq*

%% PLOT 

% i am using the colorbrewer colormaps: https://de.mathworks.com/matlabcentral/fileexchange/45208-colorbrewer-attractive-and-distinctive-colormaps
% if you want to export the figure transparently, you will also need this toolbox: https://www.mathworks.com/matlabcentral/fileexchange/23629-export_fig

addpath Y:\AttEmo_iEEG\Toolboxes\colorschemes
addpath Y:\AttEmo_iEEG\Toolboxes\export_fig

cd C:\Users\enyaw\sciebo\AttEmo_Material\resection_freq\Paper\images\GBA_raw

figure('Color', [1 1 1]);
cfg = [];
cfg.parameter = 'powspctrm';
cfg.layout    = 'biosemi128.lay';
cfg.colormap = flipud(colormap(brewermap([],'Spectral')));
cfg.colorbar = 'no';
cfg.layout = 'biosemi128.lay';
cfg.comment = 'no';
cfg.zlim = [-0.03 0.03];
cfg.ylim = [35 90];

%%%% topos negative

%%%% topos negative
cfg.xlim = [-0.6 -0.1];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr_base','-dpng', '-transparent', '-r300');


cfg.xlim = [0 0.1];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr1','-dpng', '-transparent', '-r300');

cfg.xlim = [0.1 0.2];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr2', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.2 0.3];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr3', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.3 0.4];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr4', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.4 0.5];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr5', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.5 0.6];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr6', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.6 0.7];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr7', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.7 0.8];
ft_topoplotTFR(cfg, f_neg_AHR);
export_fig('topo_neg_ahr8', '-dpng', '-transparent', '-r300');

% AHL
cfg.xlim = [-0.6 -0.1];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl_base', '-dpng', '-transparent', '-r300');


cfg.xlim = [0 0.1];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl1', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.1 0.2];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl2', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.2 0.3];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl3', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.3 0.4];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl4', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.4 0.5];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl5', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.5 0.6];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl6', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.6 0.7];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl7', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.7 0.8];
ft_topoplotTFR(cfg, f_neg_AHL);
export_fig('topo_neg_ahl8', '-dpng', '-transparent', '-r300');

% CON
cfg.xlim = [-0.6 -0.1];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con_base', '-dpng', '-transparent', '-r300');

cfg.xlim = [0 0.1];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con1', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.1 0.2];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con2', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.2 0.3];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con3', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.3 0.4];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con4', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.4 0.5];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con5', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.5 0.6];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con6', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.6 0.7];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con7', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.7 0.8];
ft_topoplotTFR(cfg, f_neg_CON);
export_fig('topo_neg_con8', '-dpng', '-transparent', '-r300');


%%%% topos neutral
cfg.xlim = [-0.6 -0.1];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr_base', '-dpng', '-transparent', '-r300');


cfg.xlim = [0 0.1];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr1', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.1 0.2];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr2', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.2 0.3];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr3', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.3 0.4];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr4', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.4 0.5];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr5', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.5 0.6];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr6', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.6 0.7];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr7', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.7 0.8];
ft_topoplotTFR(cfg, f_ntr_AHR);
export_fig('topo_ntr_ahr8', '-dpng', '-transparent', '-r300');

% AHL
cfg.xlim = [-0.6 -0.1];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl_base', '-dpng', '-transparent', '-r300');


cfg.xlim = [0 0.1];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl1', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.1 0.2];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl2', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.2 0.3];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl3', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.3 0.4];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl4', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.4 0.5];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl5', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.5 0.6];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl6', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.6 0.7];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl7', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.7 0.8];
ft_topoplotTFR(cfg, f_ntr_AHL);
export_fig('topo_ntr_ahl8', '-dpng', '-transparent', '-r300');

% CON
cfg.xlim = [-0.6 -0.1];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con_base', '-dpng', '-transparent', '-r300');

cfg.xlim = [0 0.1];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con1', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.1 0.2];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con2', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.2 0.3];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con3', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.3 0.4];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con4', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.4 0.5];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con5', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.5 0.6];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con6', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.6 0.7];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con7', '-dpng', '-transparent', '-r300');

cfg.xlim = [0.7 0.8];
ft_topoplotTFR(cfg, f_ntr_CON);
export_fig('topo_ntr_con8', '-dpng', '-transparent', '-r300');



%%%%%%
% plot tf-matrices, divide them for frontal, left, right, and posterior quarters of the scalp

figure('Color', [1 1 1]);
cfg = [];
cfg.parameter = 'powspctrm';
cfg.layout    = 'biosemi128.lay';
cfg.colormap = flipud(colormap(brewermap([],'Spectral')));
cfg.colorbar = 'no';
cfg.comment = 'no';
cfg.zlim = [-0.03 0.03];
cfg.ylim = [35 90];
cfg.xlim = [-0.6 0.8];
cfg.title = ' ';
xticks([-0.6 0 0.8 ]);
yticks([35 90]);
set(gca,'fontsize',40)

% front
cfg.channel = {'C*', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D6', 'D7', 'D8','D9', 'D10', 'D11', 'D12', 'D13', 'D14', 'D15', 'D19', 'D21', 'D23', 'B1', 'B27', 'B28', 'B29', 'B30', 'B31', 'B32', 'B20', 'B22', 'B24', 'B26'};
ft_singleplotTFR(cfg, f_neg_AHR);
export_fig('front_neg_ahr','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_ntr_AHR);
export_fig('front_ntr_ahr','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_neg_AHL);
export_fig('front_neg_ahl','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_ntr_AHL);
export_fig('front_ntr_ahl','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_neg_CON);
export_fig('front_neg_con','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_ntr_CON);
export_fig('front_ntr_con','-dpng', '-transparent', '-r300');

% post
cfg.channel = {'A*', 'D16', 'D17', 'D18', 'D19', 'D20', 'D21', 'D22', 'D23', 'D24', 'D18', 'D20', 'D22', 'B2' , 'B3', 'B4', 'B5','B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14', 'B15', 'B16', 'B17', 'B18', 'B19', 'B21', 'B23', 'B25'};
ft_singleplotTFR(cfg, f_neg_AHR);
export_fig('post_neg_ahr','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_ntr_AHR);
export_fig('post_ntr_ahr','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_neg_AHL);
export_fig('post_neg_ahl','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_ntr_AHL);
export_fig('post_ntr_ahl','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_neg_CON);
export_fig('post_neg_con','-dpng', '-transparent', '-r300');

ft_singleplotTFR(cfg, f_ntr_CON);
export_fig('post_ntr_con','-dpng', '-transparent', '-r300');

