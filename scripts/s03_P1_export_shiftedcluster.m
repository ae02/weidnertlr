%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Bielefeld University
%%%%% Enya Weidner, 2024
%%%%% % this script exports the ERP data to a text file that you can read in with
% R to perform statistical analyses. the code exports single-trials data,
% that's why it is important to specify the option "cfg.keeptrials = yes"
% in the timelock analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% because of internal codes, the group codes are different from the paper
% con = HC, ahr = rTLR, ahl = lTLR

addpath C:\Users\eweidner\sciebo\AttEmo_Material\
ft_startup

clean_AHR = load('..\data_clean\cleaned_AHR.mat');
clean_CON = load('..\cleaned_CON.mat'); %or reject until i have cleaned the tf data again
clean_AHL = load('..\data_clean\cleaned_AHL.mat');


%% ERP analysis
% i am not pre-allocating memory here but you can do this if you want the
% script to run a bit more efficiently

% rTLR
for VP = 1:length(clean_AHR.cleaned)
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    cfg.demean          = 'yes';
    cfg.baselinewindow  = [-0.2 0];
    cleaned_AHR         = ft_preprocessing(cfg, clean_AHR.cleaned{VP});
    cfg                 = [];
    data                = cleaned_AHR;
    cfg.keeptrials      = 'yes';
    cfg.trials          = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_AHR{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials          = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_AHR{VP} = ft_timelockanalysis(cfg, data);
    clear cleaned_AHR;
end

% lTLR
for VP = 1:length(clean_AHL.cleaned)
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    cfg.demean          = 'yes';
    cfg.baselinewindow  = [-0.2 0];
    cleaned_AHL         = ft_preprocessing(cfg, clean_AHL.cleaned{VP});
    cfg                 = [];
    data                = cleaned_AHL;
    cfg.keeptrials      = 'yes';
    cfg.trials          = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_AHL{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials          = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_AHL{VP} = ft_timelockanalysis(cfg, data);
    clear cleaned_AHL;
end

% HC
for VP = 1:length(clean_CON.cleaned)
    cfg                 = [];
    cfg.lpfilter        = 'yes';
    cfg.lpfreq          = 40;
    cfg.demean          = 'yes';
    cfg.baselinewindow  = [-0.2 0];
    cleaned_CON         = ft_preprocessing(cfg, clean_CON.cleaned{VP});
    cfg                 = [];
    data                = cleaned_CON;
    cfg.keeptrials      = 'yes';
    cfg.trials          = data.trialinfo==6 | data.trialinfo==106;
    timelock_ntr_CON{VP} = ft_timelockanalysis(cfg, data);
    cfg.trials          = data.trialinfo==7 | data.trialinfo==107;
    timelock_neg_CON{VP} = ft_timelockanalysis(cfg, data); 
    clear cleaned_CON;
end



%% EXPORT including info about trial, subject, hemipshere, group and emotion

cd ..\single_trials

% define time windows and channels for the components beforehand
P1_time = [0.09 0.13];

P1_chan_left = {'D24', 'D25', 'D32', 'D31'};
P1_chan_right = {'B15', 'B14', 'B10', 'B11'};

%% P1

% HC
idx = 0;
P1_left = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg             = [];
    cfg.channel     = P1_chan_left;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_CON{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo % 1 = negative, 2 = neutral
    P1_neg(1:length(data_neg.trial),4) = 1; % group % 1 = hc, 2 = lTLR, 3 = rTLR
    P1_neg(1:length(data_neg.trial),5) = 1; % hemi % 1 = left, 2 = right
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo % 1 = negative, 2 = neutral
    P1_ntr(1:length(data_ntr.trial),4) = 1; % group % 1 = hc, 2 = lTLR, 3 = rTLR
    P1_ntr(1:length(data_ntr.trial),5) = 1; % hemi % 1 = left, 2 = right
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_left = [P1_left; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

idx = 0;
P1_right = [];
for VP = [1:17 19] % con
    idx = idx +1;

    cfg                     = [];
    cfg.channel             = P1_chan_right;
    cfg.latency             = P1_time;
    cfg.avgoverchan         = 'yes';
    cfg.avgovertime         = 'yes';
    data_neg                = ft_selectdata(cfg, timelock_neg_CON{VP});
    data_ntr                = ft_selectdata(cfg, timelock_ntr_CON{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 1; % group
    P1_neg(1:length(data_neg.trial),5) = 2; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    P1_ntr(1:length(data_ntr.trial),4) = 1; % group 
    P1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_right = [P1_right; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

P1_con = array2table(num2cell([P1_left; P1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% lTLR
idx = 0;
P1_left = [];
for VP = [1:3 5:14 16:20] 
    idx = idx +1;
 
    cfg             = [];
    cfg.channel     = P1_chan_left;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+36; % subjID % i am adding a random number > 21 to avoid having the same subj IDs
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 2; % group
    P1_neg(1:length(data_neg.trial),5) = 1; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    P1_ntr(1:length(data_ntr.trial),4) = 2; % group
    P1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_left = [P1_left; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

idx = 0;
P1_right = [];
for VP = [1:3 5:14 16:20] % ahl
    idx = idx +1;
   
    cfg             = [];
    cfg.channel     = P1_chan_right;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHL{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHL{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+36; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 2; % group
    P1_neg(1:length(data_neg.trial),5) = 2; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+36; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    P1_ntr(1:length(data_ntr.trial),4) = 2; % group 
    P1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_right = [P1_right; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

P1_ahl = array2table(num2cell([P1_left; P1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});


% rTLR
idx = 0;
P1_left = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = P1_chan_left;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 3; % group
    P1_neg(1:length(data_neg.trial),5) = 1; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo
    P1_ntr(1:length(data_ntr.trial),4) = 3; % group
    P1_ntr(1:length(data_ntr.trial),5) = 1; % hemi
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_left = [P1_left; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

idx = 0;
P1_right = [];
for VP = [1:3 5:8 10:20] % ahr
    idx = idx +1;

    cfg             = [];
    cfg.channel     = P1_chan_right;
    cfg.latency     = P1_time;
    cfg.avgoverchan = 'yes';
    cfg.avgovertime = 'yes';
    data_neg        = ft_selectdata(cfg, timelock_neg_AHR{VP});
    data_ntr        = ft_selectdata(cfg, timelock_ntr_AHR{VP});

    P1_neg(:,3) = data_neg.trial;
    P1_neg(1:length(data_neg.trial),1) = VP+60; % subjID
    P1_neg(1:length(data_neg.trial),2) = 1; % emo
    P1_neg(1:length(data_neg.trial),4) = 3; % group
    P1_neg(1:length(data_neg.trial),5) = 2; % hemi
    P1_neg(1:length(data_neg.trial),6) = 1:length(data_neg.trial); % trial

    P1_ntr(:,3) = data_ntr.trial;
    P1_ntr(1:length(data_ntr.trial),1) = VP+60; % subjID
    P1_ntr(1:length(data_ntr.trial),2) = 2; % emo 
    P1_ntr(1:length(data_ntr.trial),4) = 3; % group 
    P1_ntr(1:length(data_ntr.trial),5) = 2; % hemi 
    P1_ntr(1:length(data_ntr.trial),6) = 1:length(data_ntr.trial); % trial

    P1_right = [P1_right; P1_neg; P1_ntr];

    clear P1_neg
    clear P1_ntr
end

P1_ahr = array2table(num2cell([P1_left; P1_right]), 'VariableNames', {'subject','emo','value','group','hemi', 'trial'});

P1 = [P1_con; P1_ahl; P1_ahr];

writetable(P1, 'P1_singletrials_shiftedcluster.csv')


