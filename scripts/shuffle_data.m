
% Function to shuffle the specified factor
function shuffled_data = shuffle_data(data, factor)
    subjects = unique(data.subj);
    shuffled_data = data;

    for s = 1:length(subjects)
        subject_data = data(data.subj == subjects(s), :);
        unique_conditions = unique(subject_data.(factor));

        shuffled_conditions = unique_conditions(randperm(length(unique_conditions)));
        for i = 1:length(unique_conditions)
            shuffled_data.(factor)(subject_data.subj == subjects(s) & subject_data.(factor) == unique_conditions(i)) = shuffled_conditions(i);
        end
    end
end